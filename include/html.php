<?php
// Not needed now -require_once "gzdoc.php"; // No blank lines before this one!

require("content-admin/News.php");

	/**
	 * Codifica el texto dado sustituyendo los caracteres especiales de HTML
	 * por su equivalente.
	 */
	function html_decode($encodedhtmltext) {
	  //$traslation_table = get_html_translation_table(HTML_ENTITIES);
	  $translation_table = array("&lt;" => "<" ,"&gt;" => ">", "&amp;" => "&");
	  //echo "$encodedhtmltext >"."$translation_table"."<";
	  //$translation_table = array_flip($translation_table);
	  $result = strtr($encodedhtmltext , $translation_table);
	  return $result;
	}

function colorIfSelected($thisSection="wrongvalue", $menuItem="wrongvaluetoo") {
	if ($thisSection == $menuItem) {
		echo 'bgcolor="#e4e4ff"';//#ccddee";
	} else {
        	//echo "debug='seccion=$thisSection item=$menuItem' ";
        }
}

function n_mail( $from, $to , $subject,$core_msg, $add_header) { 
 
 $cmd_line=sprintf("/usr/sbin/sendmail -r %s -t -i",$from); 
 $fp=popen($cmd_line,"w"); 
 $temp=sprintf("To: %s\n",$to); 
 $len=strlen($temp); 
 $ret_p=fputs($fp,$temp,$len); 
 $temp=sprintf("Subject: %s\n",$subject); 
 $len=strlen($temp); 
 $ret_p=fputs($fp,$temp,$len); 
 if ($add_header != "")	       { 
   $temp=sprintf("%s\n",$add_header); 
   $len=strlen($temp); 
   $ret_p=fputs($fp,$temp,$len); 
 } 
 $temp=sprintf("\n%s\n",$core_msg); 
 $len=strlen($temp); 
 $ret_p=fputs($fp,$temp,$len); 
 pclose($fp); 
} 

function html_page_header ($title = '', $section = "none") {
	?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
	<head><title><?php echo $title; ?></title>
	<style type="text/css">
	<!--
	a.icon {
		color: #2233aa;
		font-weight: bold;
		text-decoration: none;
	}
	a.menu {
		color: #5566dd;
		font-weight: bold;
		text-decoration: none;
	}
	--></style>
	<link REL="icon" HREF="/images/gnome-db.png" TYPE="image/png">
	</head>
	<body text="#000000" bgcolor="#cccccc">
	<table border=0 cellpadding="0" cellspacing="3" width="100%">
	<tr>
		<td>
			<table border=0 cellpadding="0" cellspacing="3" width="100%">
			<tr>
				<td bgcolor="#777777">
					<table  border="0" cellpadding="0" cellspacing="2" width="100%" align="center">
					<tr>
						<td bgcolor="#ffffff" colspan="0" width="100%">
							<table cellspacing="0" width="100%">
							<tr>
								<td>
								<img src="/images/gnome-db.png" ALT="GNOME-DB"  border="0"></td>
<td width="100%"><h1>GNOME-DB<br><font size="-1">GNOME database integration</font></h1>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td rowspan="0" >
			<table border=0 cellpadding=0 cellspacing="3" width="100%">
			<tr>
				<td valign="top" align="left" width="20%">
					<table border=0 cellpadding=0 cellspacing=0 width="100%">
					<tr>
						<td valign="top">
							<table border=0 cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td bgcolor="#777777" valign="top">
									<table  border="0" cellpadding="5" cellspacing="2" width="100%" align="center">
									<tr>
										<td  valign="top" bgcolor="#ffffff" nowrap>
											<center>
											<table cellspacing="4" align="left" border="0" width="100%">
<tr bgcolor="#99aacc"><td align="center" colspan="3"><font color="#ffffff"><b>
Information</b></font></td></tr>
											<tr><td width="10" rowspan="7">&nbsp;</td><td align="center" <?php colorIfSelected($section,'main') ?>><a class="menu" href="/"><b>Main</b></a></td><td width="10" rowspan="7">&nbsp;</td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'news') ?>><a class="menu"  href="/news.php">News</a></td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'download') ?>><a class="menu"  href="/download.php">Download</a></td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'documentation') ?>><a class="menu" href="/docs/">Documentation</a></td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'applications') ?>><a class="menu" href="/apps.php">Applications</a></td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'screenshots') ?>><a class="menu" href="/screenshots.php">Screenshots</a></td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'thanksto') ?>><a class="menu" href="/thanks.php">Thanks to...</a></td></tr>

<tr><td>&nbsp;</td></tr>
<tr bgcolor="#99aacc"><td align="center" colspan="3"><font color="#ffffff"><b>
Development</b></font></td></tr>
											<tr><td width="10" rowspan="7">&nbsp;</td><td align="center" <?php colorIfSelected($section,'contribute') ?>><a class="menu" href="/dev/index.php">Contribute</a></td><td width="10" rowspan="7">&nbsp;</td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'people') ?>><a class="menu" href="/dev/people.php">Team</a></td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'mailinglists') ?>><a class="menu" href="/dev/mailing_lists.php">Mailing Lists</a></td></tr>
											<tr><td align="center"<?php colorIfSelected($section,'cvs') ?>><a class="menu" href="http://cvs.gnome.org/bonsai/">Browse the Code</a><br>
    <font size="-2">
      <a class="menu" href="http://cvs.gnome.org/bonsai/rview.cgi?cvsroot=/cvs/gnome&amp;dir=libgda">libgda</a><br>
      <a class="menu" href="http://cvs.gnome.org/bonsai/rview.cgi?cvsroot=/cvs/gnome&amp;dir=libgnomedb">libgnomedb</a><br>
      <a class="menu" href="http://cvs.gnome.org/bonsai/rview.cgi?cvsroot=/cvs/gnome&amp;dir=mergeant">mergeant</a><br>
    </font>
</td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'bugzilla') ?>><a class="menu" href="http://bugzilla.gnome.org">Report a bug</a></td></tr>
											<tr><td align="center" <?php colorIfSelected($section,'commits') ?>><a class="menu" href="/dev/commits.php">CVS Activity</a></td></tr>
											</table>
											</center>
										</td>
									</tr>
									</table>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
				<td valign="top" >
					<table border=0 cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td bgcolor="#777777" valign="top" >
							<table  border="0" cellpadding="5" cellspacing="2" width="100%" align="center">
							<tr>
								<td valign="top" bgcolor="#ffffff">
	<?php
	echo "<h1>$title</h1>";
}

function html_page_footer ($lastModifiedTimestamp="Not specified") {
	?>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<center><p>
	<small>
		<?php 
		  if ($lastModifiedTimestamp != "Not specified") {
		    $lastModifiedDate=
		      date("dS of F Y h:i:s A",$lastModifiedTimestamp);
		    print "Last updated: $lastModifiedDate<br>";

		  }
		?>
		All comments, suggestions, etc to
		<a href="mailto:webmaster@gnome-db.org">webmaster@gnome-db.org</a>
	</small>
	<p>
	<a href="http://validator.w3.org/check/referer">
	<img src="/images/valid-html401.png" height="31" width="88" border="0" alt="Valid HTML 4.01!">
	</a>
	</center>
	</body>
	</html>

<?php
	// Not needed - deflate in apache GzDocOut ();
}
?>
