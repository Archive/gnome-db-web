<?php
require('html.php');

html_page_header('GNOME-DB/libgda 0.2.96 released');

?>
<pre>
GNOME-DB/libGDA 0.2.96 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase, SQLite and ODBC data sources. Apart from this development
framework, they also include several applications and utilities which make
them quite suitable for many database-related taks.

This is hopefully the last stable release for GNOME 1.4. Next releases will
be, if all goes well, for GNOME 2.

Changes since last version (0.2.95):

	* Fixed aggregates schema query in PostgreSQL provider (Daniel).

	* New translations: no (kmaraas), fr (redfox), pt (dnloreto)
	
To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml1, Bonobo >= 1.0.x
* gnome-db: libgda and dependencies, gnome-libs

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
