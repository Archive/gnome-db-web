<?php
require('html.php');

html_page_header('libgda/libgnomedb/gnome-db 0.8.191 released');

?>
<pre>
libgda/libgnomedb/gnome-db 0.8.191 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite and ODBC data sources.
gnome-db is a set of visual applications that let you access from a nice
UI all the features provided in libgda/libgnomedb.

This is another preview release of the GNOME 2 versions of these 3 projects, and
likely to be the last one before the final gASQL/gnome-db merging, which is
now showing in your nearest CVS server.

libgda 0.8.191
--------------

 - Added framework for editable data models (rodrigo)
 - Made MySQL data models editable (rodrigo)
 - Extended data model API (rodrigo)
 - Fixed configuration file write support (gonzalo)
 - Removed obsolete GdaRecordset class (gonzalo)
 - Removed obsolete GdaField class (gonzalo)
 - Added GDA_COMMAND_TYPE_SCHEMA (gonzalo)
 - Added GdaDataModelHash class (gonzalo)
 - More API documentation (gonzalo, rodrigo)
 - Removed unneeded dependencies from libgda.pc (rodrigo)
 - Added support for creating and droping databases
   in the providers' API (rodrigo)
 - Added new connection features (rodrigo)
 - Extended GdaValue API (gonzalo)
 - Added GdaSelect class, for doing SELECT's on data
   models (rodrigo)
 - Extended test suite (gonzalo, rodrigo)
 - Added GdaXmlConnection class (rodrigo)
 - Updated translations:
        - da (olau)
	- es (pablodc)
	- fr (redfox)
	- no (kmaraas)
	- pt (dnloreto)
	- sk (stano)
	- vi (pablo)

libgnomedb 0.8.191
------------------

 - Fixed memory leak in model management (gonzalo)
 - Moved database: monikers from gnome-db (rodrigo)
 - Updated translations:
        - da (olau)
	- fr (redfox)
	- pt (dnloreto)
	- vi (pablo)

gnome-db 0.8.191
----------------

 - Fixed activation of the configuration tab (rodrigo)
 - Fixed referencing problems in component factory (rodrigo)
 - Moved database: monikers to libgnomedb (rodrigo)
 - Updated translations:
        - da (olau)
	- fr (redfox)
	- no (kmaraas)
	- sk (stano)
	- vi (pablo)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.8.191/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* gnome-db: libgda and dependencies, libgnome/ui, libglade, libbonoboui.

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
