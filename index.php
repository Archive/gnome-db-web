<?php
require('html.php');
html_page_header('',"main");
$news = new News();
?>

<table cellspacing="10">
<tr>
<td valign="top">
	<p>
		The <b>GNOME-DB</b> project aims to provide a free
		unified data access architecture to the <a
		href="http://www.gnome.org">GNOME</a> project.
		GNOME-DB is useful for any application that accesses
		persistent data (not only databases, but data), since
		it now contains a pretty good data management API. 

	<p>
		GNOME-DB consists of the following components:
		<ul>
		<li><b>mergeant</b>: Front-end for database
		administrators and database application developers.
		<li><b>libgnomedb</b>: Database Widget Library. These
		widgets are integrated with the latest versions of
		<a href="http://glade.gnome.org/">glade</a>.
		<li><b>libgda</b>: data abstraction layer. It can
		manage data stored in databases or XML files and it
		can be used by <b>non-GNOME applications</b>.
		</ul>

     <table cellpadding="4">

  <tr><td>
  <center>
  <table border="0" width="75%">
  <tr><td bgcolor="#99aacc" align="center"><font color="#ffffff" face="verdana,arial,helvetica"><b>
  News</b></font></td></tr>
  <tr><td>
  <?php
     $news->print_latest(4);?>
  </td></tr>
  </table>
  </center></td></tr>
  <tr><td align="center" valign="middle">
  <a href="news.php">Older news ...</a>
  </table>

<table cellspacing="4" cellpadding="4" width="95%">
    <tr>
       <td bgcolor="#cccccc" colspan="2"><font face="verdana,arial,helvetica"><b>
	  Download</b></font></td></tr>
    <tr>
      <td align="center" valign="top">
	  <a href="http://ftp.gnome.org/pub/GNOME/sources/" class="icon">
	     <img alt="tar.gz" src="images/gnome-compressed.jpeg" border="0"><br>
	     Sources<br>tarball</a></td>
      <tr>
       <td colspan="2">
       For more information visit the <a href="download.php">download page</a></td></tr>
    </table>
    <table cellspacing="4" cellpadding="4" width="95%">
    <tr>
       <td bgcolor="#cccccc" colspan="3"><font face="verdana,arial,helvetica"><b>
	  Documentation</b></font></td></tr>
    <tr>
       <td valign="top" align="center">
	  <a href="http://www.gnome-db.org/docs/libgda/index.html" class="icon">
	     <img alt="libgda manual" src="images/tfisher-book3.jpeg" border="0"><br>
	     libgda manual</a></td>
       <td valign="top" align="center">
	 <a href="/docs/libgnomedb/index.html" class="icon">
	   <img alt="libgnomedb manual" src="images/tfisher-book3.jpeg" border="0"><br>
	   libgnomedb manual</a></td>
       <td valign="top" align="center">
	  <a href="http://www.gnome-db.org/docs/mergeant/index.html" class="icon">
	     <img alt="mergeant manual" src="images/tfisher-book3.jpeg" border="0"><br>
	     Mergeant manual</a> </td></tr>
    <tr><td colspan="3">
	  For more information visit the <a href="docs/">documentation page</a></td></tr>
    </table> 
    <table cellspacing="4" cellpadding="4" width="95%">
    <tr>
       <td bgcolor="#cccccc" colspan="3"><font face="verdana,arial,helvetica"><b>
	  Development</b></font></td>
    </tr>
    <tr>
      <td valign="top" align="center">
        <a href="/dev/index.php" class="icon">
	  <img alt="Constribute" src="images/gnome-devel.png" border="0"><br>
	  Contribute</a>
      </td>
      <td valign="top" align="center">
        <a href="/dev/commits.php" class="icon">
	  <img alt="CVS Activity" src="images/gnome-devel.png" border="0"><br>
	  CVS Activity</a>
      </td>
      <td valign="top" align="center">
        <a href="http://bugzilla.gnome.org" class="icon">
	  <img alt="Report a bug" src="images/gnome-devel.png" border="0"><br>
	  Report a bug</a>
      </td>
    </tr>
    <tr><td colspan="3">
	  For more information visit the <a href="dev/">development page</a></td></tr>
    <tr>
       <td bgcolor="#cccccc" colspan="3"><font face="verdana,arial,helvetica"><b>
	  License</b></font></td></tr>
    <tr><td colspan="3">
      <p>
	    Mergeant, libgnomedb and libgda are released under
	    the terms of the <b>GPL/LGPL licenses</b>. That is,
	    applications included in them are subject to the GPL
	    license, which means that any work derived from them
	    must also conform to the same license, whereas
	    libraries are released under the terms of the LGPL
	    (GNU Library General Public License), to allow any
	    type of application to make use of them. 
    </td>
    </tr>
    </table>

</td>

</tr>
</table>

<?php
$lastModifiedTime = filemtime('index.php');
html_page_footer($lastModifiedTime);

?>






