<?php
require('html.php');

html_page_header('GNOME-DB 0.2.1 released');

?>
<pre>
GNOME-DB/libGDA 0.2.1 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
it also includes several applications and utilities which make it
quite suitable for many database-related taks.

This release is the first one in a series of more regular releases, since
we're approaching a good deal of performance/stability/functionality, and we
want people to be able to easily follow the development.

* Added/Implemented stuff:
	Better Bonobo support - now works great with Bonobo! (Rodrigo)
	Packages for Debian Potato (Holger)
	Portuguese page for the web site (Cleber)
	Started implementation for database: monikers (Rodrigo)
	Started PrimeBase data source provider (Holger)
	Started Perl bindings for libGDA (Rodrigo)

* Bugs fixed:
	Bonobo toolbar/menubar now works perfectly well (at last)
	Some build problems in different configurations solved

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf >= 0.9, libxml
* gnome-db: libgda and dependencies, Bonobo >= 0.29, gnome-libs, libgal, gtkhtml

GNOME-DB web server (http://www.gnome-db.org) is now down and will be in the
same state for some days, so you can only download source tarballs from
ftp://ftp.gnome.org/pub/GNOME/stable/sources/gnome-db/, and, if you need
any help in compiling/installing/using the software, please subscribe
to the GNOME-DB mailing list (http://mail.gnome.org/mailman/listinfo/gnome-db-list)
</pre>
<?php

html_page_footer('');

?>
