<?php
require('html.php');

html_page_header('GNOME-DB/libgda/gASQL 0.2.92 released');

?>
<pre>
GNOME-DB/libGDA/gASQL 0.2.92 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
they also include several applications and utilities which make them
quite suitable for many database-related taks.
gASQL is a database administration tool based on libgda and GNOME-DB, featuring
visual tools for managing any database supported by libgda/GNOME-DB.

This release is Yet-Another-Bugfix-Release, because we broke the tarballs
generation with the SQLite sources integration. So, everybody using the tarballs
or people wanting to package this software should upgrade.

Changes:

	* version-based header installation, to allow parallel installation
	  of the stable and the GNOME 2 versions of our development
	  libraries (Rodrigo)

Bugs fixed:

	* fixed problems in tarballs with SQLite sources (Carlos)

Now in development:

	* Perl scripting support in the front-end
	* CORBA-based report engine

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml1, Bonobo >= 1.0.x
* gnome-db: libgda and dependencies, gnome-libs

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
