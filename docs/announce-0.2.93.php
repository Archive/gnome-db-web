<?php
require('html.php');

html_page_header('GNOME-DB/libgda/gASQL 0.2.93 released');

?>
<pre>
GNOME-DB/libGDA/gASQL 0.2.93 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
they also include several applications and utilities which make them
quite suitable for many database-related taks.
gASQL is a database administration tool based on libgda and GNOME-DB, featuring
visual tools for managing any database supported by libgda/GNOME-DB.

Changes:

	* New API functions: the GnomeDbGrid API has been extended to provide
	  a gnome_db_grid_get_selection, which returns a list of all selected
	  rows in the grid widget (Rodrigo)
	
	* Added binary age to libraries (Akira)

	* Refactored SQLite sources integration in libgda (Carlos)

	* Improved C++ bindings (Kuba)

	* Fixed UI test programs (Carlos)

	* Improved Sybase provider (Mike)

Bugs fixed:

	* Now RPM packages build ok (Adam, Carlos, Rodrigo)

	* Fixed detection of Perl in GNOME-DB (cjorsinger@earthlink.net)

	* Fixed configure check for Bonobo (Rodrigo)

Now in development:

	* Perl scripting support in the front-end
	* CORBA-based report engine

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml1, Bonobo >= 1.0.x
* gnome-db: libgda and dependencies, gnome-libs

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
