<?php
require('html.php');

html_page_header('GNOME-DB 0.2.2 released');

?>
<pre>
GNOME-DB/libGDA 0.2.2 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
it also includes several applications and utilities which make it
quite suitable for many database-related taks.

This release is just a bugfix release, due to some critical bugs found in
the previous release (0.2.1), so people using that version should consider
upgrading to this new version

* Bugs fixed:
	added missing function pointer to one of the ORBit-generated structs,
		which caused some CORBA operations to fail always
	much more clever default-user config generation, thus avoiding all
		the problems people have been having when upgrading

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf >= 0.9, libxml
* gnome-db: libgda and dependencies, Bonobo >= 0.29, gnome-libs, libgal, gtkhtml

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php

html_page_footer('');

?>
