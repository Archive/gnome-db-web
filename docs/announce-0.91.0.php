<?php
require('html.php');

html_page_header('libgda/libgnomedb 0.91.0 released');

?>
<pre>
libgda/libgnomedb 0.91.0 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.

This release is another milestone in the road to 1.0.

Changes in this release since 0.90.0:

libgda 0.91.0
-------------

 - Renamed default provider to XML and added some missing things (rodrigo)
 - Fixed #116758 (rodrigo)
 - Fixed x86-64 bits compilation problems (fcrozat)
 - Made basic SQL work in GdaSelect class (rodrigo)
 - Added missing LDAP flags (juergen)
 - Initial 'delete' support in the SQL parser library (gonzalo)
 - Removed compilation warnings (gonzalo)
 - Improved changes management in GdaXmlDatabase (rodrigo)
 - More API documentation (rodrigo)
 - Fixed permissions problem in gda_file_save (philippe)
 - Fixed lexer compilation problems (gonzalo)
 - Updated translations:
        - cs (mitr)
	- de (cneumair)
	- es (pablodc)
	- it (marcoc)
	- nl (adrighem)
	- pl (aflinta)
	- pt (dnloreto)
        - sv (menthos)

libgnomedb 0.91.0
-----------------

 - Extended GnomeDbLogin API (rodrigo)
 - Added a 'Create DSN' button on the GnomeDbLogin widget (rodrigo)
 - Redid UI for database properties configuration applet (rodrigo)
 - Fixed #108023 (rodrigo)
 - Some HIG-ification (apg, rodrigo)
 - Added query builder widget, to be used in Gnumeric and Abiword (rodrigo)
 - Extended GnomeDbEditor API (daniel)
 - More API documentation (rodrigo)
 - Updated translations:
        - cs (mitr)
	- de (cneumair)
	- es (pablodc)
	- nl (adrighem)
	- pl (aflinta)
	- pt (dnloreto)
        - sv (menthos)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.91.0/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
