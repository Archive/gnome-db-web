<?php
require('html.php');

html_page_header('libgda/libgnomedb/mergeant 0.8.193 released');

?>
<pre>
libgda/libgnomedb/mergeant 0.8.193 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite and ODBC data sources.
Mergeant is an end user application that makes use of libgda and libgnomedb to
allow users to easily manage their databases.

This is the first release including Mergeant, which is the result of merging
gASQL (http://gasql.sourceforge.net) and the old GNOME-DB front-end.

libgda 0.8.193
--------------

 - Added support for storing types in GdaValue's (rodrigo)
 - Updated schemas information in MySQL (rodrigo) and PostgreSQL (vivien)
 - Fixed encoding problems in PostgreSQL provider (gonzalo)
 - Set correctly number of rows in PostgreSQL non-select queries (gonzalo)
 - Added support for listing data sources and providers in libgda's
   command line configuration tool (gonzalo)
 - Fixed retrieval of fields information on the MySQL provider (rodrigo)
 - Starting port of the FreeTDS provider (holger)
 - Added FreeTDS-specific tests to the test suite (holger)
 - New detailed documentation, including migration formulae (xabier)
 - Added missing types to gda_value_stringify (holger)
 - Started IBM DB2 provider (holger)
 - Added get_server and get_server_version methods to the GdaServerProvider
   class (rodrigo), and implemented them in MySQL (rodrigo), PostgreSQL (gonzalo),
   FreeTDS (holger)
 - Fixed spec files (ben)
 - Added isolation level support to transactions (rodrigo)
 - Implemented support for binary values in GdaValue's (rodrigo)
 - Added more information to GdaFieldAttributes structure (rodrigo)
 - Added basic support for commands to default provider (rodrigo)
 - Fixed suffix names problem for running on Windows (gonzalo)
 - Propagate errors to GdaClient when opening connections (rodrigo)
 - Updated translations:
 	- ca (pablo)
 	- da (olau)
 	- de (mborchers, cneumair)
 	- es (pablodc)
 	- no (kmaraas)
	- pl (chyla)
	- pt (dnloreto)
	- ru (dmitrym)
	- sv (menthos)
	- vi (pablo)

libgnomedb 0.8.193
------------------

 - Added optional title bar to GnomeDbGrid widget (rodrigo)
 - Added 'Test DSN' option on the gnome-database-properties applet (rodrigo)
 - Added GnomeDbConnectionProperties widget from GNOME-DB (rodrigo)
 - Fixed spec files (ben)
 - Updated translations:
 	- da (olau)
	- de (chrisime, cneumair)
 	- es (pablodc)
	- no (kmaraas)
	- pl (chyla)
	- pt (dnloreto)
	- pt_BR (gdvieira)
 	- sv (menthos)
	- vi (pablo)
	- zh_CN (carton)

mergeant 0.8.193
----------------

 - Fully ported to GNOME 2 from the gASQL base source (vivien)
 - Improvements on the query and data form interfaces (vivien)
 - Added SQL window (rodrigo)
 - Reworked the plugins interfaces (vivien)
 - Updated schemas usage to match changes in libgda (vivien)
 - Added title bar to working area (rodrigo)
 - Better handling of data types (vivien)
 - Updated translations:
 	- da (olau)
 	- de (cneumair)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.8.193/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui
* mergeant: libgda/libgnomedb and dependencies

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
