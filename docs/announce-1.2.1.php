<?php
require('html.php');

html_page_header('libgda/libgnomedb 1.2.1 released');

?>
<pre>
libgda/libgnomedb 1.2.1 have been released.

libgda/libgnomedb are a complete framewok for developing
database-oriented applications, and currently allow access to PostgreSQL,
MySQL, Oracle, Sybase, SQLite, FireBird/Interbase, IBM DB2, mSQL and MS
SQL server, as well as MS Access and xBase files and ODBC data sources.
                                                                                
libgda/libgnomedb are the base of the database support in the GNOME
Office application suite, providing database access for many features in
both Gnumeric and Abiword.

Changes since libgda 1.2.0
--------------------------

 - Fixed includedir in spec file (Aaron)
 - Set row number in appended row for GdaDataModelHash (Bas)
 - Fixes in 'update_row'/'remove_row' methods for PostgreSQL provider (Bas)
 - Fixed intltool detection (Rodrigo)
 - Fixed deletion of rows in GdaDataModelHash (Bas)
 - Improve table name guessing in PostgreSQL updates (Bas)
 - Allow USER and PASSWORD parameters in MySQL connection string (Bas)
 - Removed extra emission of signals in data model base class (�lvaro)
 - Renumber rows after deletion in GdaDataModelHash (�lvaro)
 - Added "name_changed" signal in GdaTable class (�lvaro)
 - Fixed #167700 (Caolan)
 - Fixed #160666 (Rodrigo)
 - Fixed #162856 (Jorge)
 - Fixed #166286 (Caolan)
 - Fixed #166288 (Caolan)
 - Fixed #129153 (Rodrigo)
 - Fixed #167500 (Denis)
 - Fixed #168641 (Lo�c)

Changes since libgnomedb 1.2.0
------------------------------

 - Renumber rows on deletion for GtkTreeModel's (�lvaro)

Tarballs are available at
http://download.gnome.org/pub/GNOME/sources/libgda/1.2/
http://download.gnome.org/pub/GNOME/sources/libgnomedb/1.2/
                                                                                
To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade,
libbonoboui and, optionally, gtksourceview
                                                                                
You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything
you want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>