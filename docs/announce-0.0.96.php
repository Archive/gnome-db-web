<?php
require('html.php');

html_page_header('GNOME-DB 0.0.96 released');

?>
<pre>
GNOME-DB 0.0.96 (a.k.a. getting-to-0.1) has been released.

GNOME-DB is a complete framewok for developing database-oriented
applications, and actually allows access to PostgreSQL, MySQL,
Oracle and ODBC data sources. Apart from this development framework,
it also includes several applications and utilities which make it
quite suitable for many database-related taks.
This new release is another big step towards version 0.1, and includes:

* New and improved providers: the MySQL, ODBC and Oracle providers have
  been rewritten to make full use of the new libgda-server, and they are
  now stable and growing very quickly.
* Provider development framework: we rewrote the already existing
  providers in a couple of weeks thanks to using the new libgda-server,
  which means that now, adding new data source providers to the
  GNOME-DB architecture is quite easy, so we encourage people to do so for
  their preferred database.
* Debian packages: thanks to Akira TAGOH, who helped us in the addition
  of the framework needed to build Debian packages. Although, only
  PostgreSQL and MySQL providers are available under this format.
  But we are working in adding them all for the next release.
* Almost-ready-to-use Interbase provider: this is only included in the
  source distribution, but if somebody wants to try the Interbase part,
  this will help us in its improvement and bug fixing.
* Now in development (soon in your GNOME desktops):
	- XML queries, as an option to send commands to providers instead
	  of plain (and non-standard) SQL
	- report engine
	- Interbase, LDAP and DB2 providers
	- XML file format to import/export data from/to different
	  database systems
	- Python bindings for the client libraries
	- Pascal bindings for the client libraries
	- Bonobo components to integrate with GNOME Office applications

You'll need Bonobo installed in your system to run GNOME-DB. Don't
let this prevent you from installing GNOME-DB in your systems,
since although this a stable version, it's
been released as a development snapshot. As soon as Bonobo
becomes part of the GNOME stable releases, GNOME-DB 0.1 will be
released, and this will be the first ready-for-all version. And we
have Bonobo RPMs (the same version we use ourselves) on our web site,
so please don't hesitate and have a try, specially the PostgreSQL
part, which is the most complete provider. As soon as we can, Debian
packages for the Bonobo component system will be provided or pointed to.

You can download the package and find other sources of information
in http://www.gnome.org/projects/gnome-db/.
</pre>
<?php

html_page_footer('');

?>
