<?php
require('html.php');

html_page_header('GNOME-DB 0.2.3 released');

?>
<pre>
GNOME-DB/libGDA 0.2.3 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
it also includes several applications and utilities which make it
quite suitable for many database-related taks.

This release is just a compilation of new translations added because
of the inclusion of GNOME-DB/libGDA in the "5th Toe" GNOME Extra Apps
release, so don't expect too much about this new version if you were
using 0.2.2. New features are being worked on for the 0.2.4 version.

New translations: es, el, de, fr, ja, sv, sk, hu, no, fr, ko, ... (thanks
very much from the GNOME-DB team to the different translation teams)

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml
* gnome-db: libgda and dependencies, Bonobo, gnome-libs, libgal

All this stuff should be the same as shipped with GNOME 1.4 RC1

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php

html_page_footer('March 13th, 2001');

?>
