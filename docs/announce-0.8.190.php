<?php
require('html.php');

html_page_header('libgda/libgnomedb/gnome-db 0.8.190 released');

?>
<pre>
libgda/libgnomedb/gnome-db 0.8.190 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase, SQLite and ODBC data sources.
gnome-db is a set of visual applications that let you access from a nice
UI all the features provided in libgda/libgnomedb.

This is a big step forward for libgda, which has seen all its CORBA dependencies removed, resulting in a tiny library with a few dependencies (glib/libxml) offering a lot of power (data access to many different unrelated data sources). This has been done following the desires of a lot of potential libgda users (Mono, GNOME Office, etc).

libgda 0.8.190
--------------

 - Removed CORBA dependencies in the core (rodrigo, gonzalo)
 - Added more semantic information to FieldAttributes (rodrigo)
 - Added new schemas: Databases, Users, Sequences (rodrigo)
 - Implemented new schemas for PostgreSQL (gonzalo)
 - Implemented Database schema for MySQL (rodrigo)
 - Implemented Database schema for the XML provider (rodrigo)
 - Fixed #74404 (gonzalo)
 - Fixed error management (gonzalo)
 - I18N fixes (chyla)
 - New API functions for GdaConnection (rodrigo)
 - New config system allowing global settings (gonzalo)
 - Updated translations:
	- ko (cwryu)
	- no (kmaraas)
	- pl (chyla)
	- pt (dnloreto)
	- sk (stano)
	- sl (minmax)
	- sv (menthos)
	- uk (rasta)

libgnomedb 0.8.190
------------------

 - Fixed docs building (gonzalo)
 - I18N fixes (chyla)
 - New stock icons (apg)
 - Fixed GnomeDbWindow creation (murrayc)
 - Started GnomeVFS database: method (rodrigo)
 - Updated translations:
	- fr (cfergeau)
	- ko (cwryu)
	- ms (sebol)
	- no (kmaraas)
	- pl (chyla)
	- pt (dnloreto)
	- sk (stano)
	- sl (minmax)
	- sv (menthos)
	- tr (pablo)
	- vi (pablo)

gnome-db 0.8.190
----------------

 - Improvements in the SQL editor widget (rodrigo)
 - Component factory fixes (gonzalo)
 - Fixes for I18N (kmaraas, chyla)
 - Fixed documentation building (david)
 - Updated translations:
	- ko (cwryu)
	- ms (sebol)
	- no (kmaraas)
	- pl (chyla)
	- pt (dnloreto)
	- sk (stano)
	- sl (minmax)
	- sv (menthos)
	- uk (rasta)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.8.190/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* gnome-db: libgda and dependencies, libgnome/ui, libglade, libbonoboui.

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
