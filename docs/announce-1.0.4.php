<?php
require('html.php');

html_page_header('libgda/libgnomedb 1.0.4 released');

?>
<pre>
libgda/libgnomedb 1.0.4 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.

libgda/libgnomedb are the base of the database support in the GNOME Office
application suite, providing database access for many features in both
Gnumeric and Abiword.

This is a bugfix release, containing fixes for various bugs found by users
in the 1.0.3 release.

libgda 1.0.4
------------

 - Fixed connection pool problems with sharing/not sharing of connections (jon, laurent)
 - Fixed UTF-8 problems in ODBC provider (jon)
 - Fixed setting of config entries when the type has changed (laurent)
 - Fixed spec file (rodrigo)
 - Fixed splitting of SQL commands in SQLite provider (benjamin)
 - Fixed values in 'Not Null?' fields schema for PostgreSQL provider (denis)
 - Updated FreeTDS provider to latest FreeTDS API (rodrigo)
 - Updated translations:
        - ca (jordi)
	- cs (mitr)
	- de (christian)
	- en_CA (adam)
	- en_GB (dave)
	- fi (tvainika)
	- ga (alastairmck)
	- hr (rsedak)
	- id (mohammad)
	- no (kjartan)
	- pt_BR (medina)
	- sv (christian)

libgnomedb 1.0.4
----------------

 - Fixed parameter mismatch in GnomeDbCombo API (rodrigo)
 - Added missing implementation for gnome_db_config_get_component_data_model (laurent)
 - Fixed desktop icon location (julio)
 - Fixed gtk-doc comments (stephane)
 - Removed missing files in spec file (rodrigo)
 - Make sure the GUI doesnt allow data source names with spaces (rodrigo)
 - Only build documentation if gtk-doc is enabled (rodrigo)
 - Updated translations:
        - ca (jordi)
        - cs (mitr)
	- de (christian)
	- en_CA (adam)
	- en_GB (dave)
	- es (francisco)
	- fi (tvainika)
        - ga (alastairmck)
	- hr (rsedak)
	- hu (andras)
	- id (mohammad)
	- it (algol)
	- nl (vincent)
        - no (kjartan)
	- pt_BR (evandro, gustavo)
	- sr@Latin (danilo)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v1.0.4/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
