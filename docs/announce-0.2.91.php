<?php
require('html.php');

html_page_header('GNOME-DB/libgda/gASQL 0.2.91 released');

?>
<pre>
GNOME-DB/libGDA/gASQL 0.2.91 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
they also include several applications and utilities which make them
quite suitable for many database-related taks.
gASQL is a database administration tool based on libgda and GNOME-DB, featuring
visual tools for managing any database supported by libgda/GNOME-DB.

This release is mainly a bug fix release, so people are suggested to upgrade.

Changes:

	* new GdaRecordset API: new convenience functions have been added to
	  allow the retrieval of the current row in different ways (an array
	  of rows, a list of fields, a tab-delimited string) (Diego, Liam)

	* OMF integration: now, all documentation is indexed via scrollkeeper
	  (Rodrigo)

	* faster recordset loading: some tweaks have been made to the list
	  and grid widgets to make them load recordsets much faster (Rodrigo)

	* Sybase provider: the Sybase provider now should work quite
	  well (Mike)

	* MySQL provider: several fixes, to make it send error descriptions
	  in all error situations, and a better management of some data
	  types (Gonzalo, Rodrigo)

	* Log viewer works again (Rodrigo)

Bugs fixed:

	* fixed docs building auto* problems, which were breaking the package
	  building (Pavel, Rodrigo)

	* many compiling fixes, specially the one which now allows to install
	  gnome-db in other prefix different than the one used by
	  Bonobo (Gonzalo, Rodrigo)

Now in development:

	* Perl scripting support in the front-end
	* CORBA-based report engine

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml1, Bonobo >= 1.0.x
* gnome-db: libgda and dependencies, gnome-libs

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
