<?php
require('html.php');

html_page_header('Documentaci�n');
?>
	<p>
		En esta p�gina estan listados todos los documentos
		sobre GNOME-DB/libgda disponibles en castellano
	</p>
	<h3>Manuales</h3>
	<p>
		Los manuales traducidos hasta la fecha son:
		<ul>
			<li>
				Manual de GNOME-DB:
				<a href="manual/gnome-db/index.html">HTML online</a>,
				<a href="manual/gnome-db-manual-html.tar.gz">HTML tarball</a>,
				<a href="manual/gnome-db-manual.pdf">PDF</a>,
				<a href="manual/gnome-db-manual.ps">PostScript</a>
			</li>
		</ul>
	</p>
	<h3>Otros documentos</h3>
	<p>
		Puesto que algunos de los desarrolladores del proyecto
		GNOME-DB son espa�oles, hay disponibles en Internet
		distintos documentos relacionados con este proyecto:
		<ul>

			<li>transparencias de la
			<a href="http://www.es.gnome.org/documentacion/charlas/gnome-db.html">charla
			sobre GNOME-DB/libgda</a> impartida en el III Congreso
			de Hispalinux</li>

		</ul>
	</p>
<?php
html_page_footer("May 27th, 2001");

?>
