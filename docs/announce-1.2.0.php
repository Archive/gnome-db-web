<?php
require('html.php');

html_page_header('libgda/libgnomedb 1.2.0 released');

?>
<pre>
libgda/libgnomedb 1.2.0 have been released.

libgda/libgnomedb are a complete framewok for developing
database-oriented applications, and currently allow access to PostgreSQL,
MySQL, Oracle, Sybase, SQLite, FireBird/Interbase, IBM DB2, mSQL and MS
SQL server, as well as MS Access and xBase files and ODBC data sources.
                                                                                
libgda/libgnomedb are the base of the database support in the GNOME
Office application suite, providing database access for many features in
both Gnumeric and Abiword.

Changes since libgda 1.1.99
---------------------------

 - Detect correctly xbase library path (Tomasz)
 - Fixed spec file for correct building on RH (David)
 - Win32 build improvements and fixes (Alan)
 - Added missing method implementation in GdaDataModelHash (Vivien)
 - Fixed support for DATE, TIME, TIMESTAMP and DATETIME types in MySQL
   provider (Vivien)
 - Improved pkg-config modules detection (Tomasz)
 - SQL parser improvements and fixes (Dru, Vivien)
 - Fixed auto* build (Rodrigo)
 - Use " character for field names in PostgreSQL queries (Bas)
 - Updated translations:
        - cs (Miroslav)
        - en_CA (Adam)
        - en_GB (David)
        - fi (Tommi)
        - it (Marco)

Changes since libgnomedb 1.1.99
-------------------------------

 - Merged RH spec changes (Rodrigo)
 - Updated translations:
        - ka (Metin)

Tarballs are available at
http://download.gnome.org/pub/GNOME/sources/libgda/1.2/
http://download.gnome.org/pub/GNOME/sources/libgnomedb/1.2/
                                                                                
To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade,
libbonoboui and, optionally, gtksourceview
                                                                                
You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything
you want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>