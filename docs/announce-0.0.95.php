<?php
require('html.php');

html_page_header('GNOME-DB 0.0.95 released');

?>
<pre>
GNOME-DB 0.0.95 (a.k.a. GNOME-DB meets Bonobo) has been released.

GNOME-DB is a complete framewok for developing database-oriented
applications, and actually allows access to PostgreSQL, MySQL,
Oracle and ODBC data sources.
This new release is a big step from previous versions, and includes:

* Bonobo config components: these are BonoboControls used for
  administering your databases from GNOME-DB. Although, only PostgreSQL
  and ODBC config components are provided. The rest are on the way.	
* Provider development framework: this is a library which implements
  all the CORBA-related code on the server part, so now it is
  possible to easily add new backends for accessing all kinds of
  data sources (mSQL, Informix, POP3/IMAP, GNU SQL Server, ...).
  This library has just been started, so it would be great if
  somebody wanted to add a new provider to help us to fully test it.
* GDA Manager: this is a tool to configure all your GNOME-DB
  environment and to load the config components. It now allows
  also for system-wide configuration, as well as local.
* Bonobo components: a couple of Bonobo components are provided,
  a database browser, and a grid (in early stages though), which can
  be easily embedded into other Bonobo-aware applications. More
  components to come.
* C++ bindings: an initial snapshot of the C++ bindings for the
  client libraries. As it is in beta state, we'd like people interested
  in developing C++ apps based on GNOME-DB to have a try at them.
* New and improved widgets: the client part of GNOME-DB provides a
  library which contains several database-oriented widgets to allow
  an easy integration in GNOME apps. The already existing widgets
  (grid, browser, connect dialog, error viewer, combo box, ...) have
  been improved in several ways, and others have been added, such as
  the GnomeDbLogViewer, the GnomeDbDsnConfig, ...
* Support in Glade: the Glade developers have added support for the
  GNOME-DB widgets in Glade. This is included in glade-0.5.9.
* Translations: Dannish, German, Greek, Spanish, French, Irish
  (Gaeilge), Galician, Hungarian, Japanese, Norwegian, Polish,
  Russian, Swedish and Ukranian. Also, the GNOME-DB manual translation
  to Spanish has been started by the gnome-es team
  (http://www.croftj.net/~barreiro/spanish/gnome-es/)
* millions of bugs have been fixed. Now all the CORBA part seems quite
  stable

You'll need Bonobo installed on your system to run GNOME-DB. Don't
protest too much about this, since although this a stable version, it's
been released as a development snapshot. As soon as Bonobo
becomes part of the GNOME stable releases, GNOME-DB 0.1 will be
released, and this will be the first ready-for-all version. And we
have Bonobo RPMs (the same version we use ourselves) on our web site,
so please don't hesitate and have a try, specially the PostgreSQL
part, which is the most complete provider.

You can download the package and find other sources of information
in http://www.gnome.org/gnome-db/.
</pre>
<?php

html_page_footer('');

?>
