<?php
require('html.php');

html_page_header('libgda/libgnomedb 1.0.3 released');

?>
<pre>
libgda/libgnomedb 1.0.3 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.

libgda/libgnomedb are the base of the database support in the GNOME Office
application suite, providing database access for many features in both
Gnumeric and Abiword.

This is a bugfix release, containing fixes for various bugs found by users
in the 1.0.2 release.

libgda 1.0.3
------------

 - Backported gda_data_model_foreach from HEAD (gonzalo)
 - Python bindings related issues (jon)
 - Fixed documentation typos (rodrigo)
 - Fixed C99'isms (rodrigo)
 - Fixed replacement of files in gda_file_save (laurent)
 - Fixed startup commands execution in SQLite provider (nikolai)
 - Updated translations:
        - es (serrador)
        - nl (adrighem)
        - pt (dnloreto)

libgnomedb 1.0.3
----------------

 - Fixed sorting in grid widget (marius)
 - Fixed selection signal emission in grid widget (rodrigo)
 - Fixed libglade module compilation problems (rodrigo)
 - Fixed compilation problems on Debian (jdassen)
 - Use a grid instead of a list in form widget (jon)
 - Updated translations:
        - ja (aihana)
        - nl (adrighem)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v1.0.3/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
