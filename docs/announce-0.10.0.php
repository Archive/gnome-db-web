<?php
require('html.php');

html_page_header('libgda/libgnomedb/mergeant 0.10.0 released');

?>
<pre>
libgda/libgnomedb/mergeant 0.9.0 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase and MS SQL server, as well as MS Access and xBase
files and ODBC data sources.
Mergeant is an end user application that makes use of libgda and libgnomedb to
allow users to easily manage their databases.

This release is the next in a series that will result in 1.0, which is what GNOME-DB
developers are already working on. For this reason, we need users and developers
to use it and report any problem/suggestion.

Changes in this release since 0.9.0:

libgda 0.10.0
-------------

 - Added support for PostgreSQL 7.3 new features (vivien, gonzalo)
 - Added LANGUAGES schema to set of supported schemas (rodrigo)
 - Fixed TABLES schema retrieval on Oracle (rodrigo)
 - Fixed translatable strings (menthos, holger)
 - Improvements on FreeTDS provider (holger)
 - Added changes notifications in data models (rodrigo)
 - Added new schemas to test suite (rodrigo)
 - Fixed GdaTable class (rodrigo)
 - First working version of the MDB (MS Access files) provider (rodrigo)
 - Fixed retrieval of NUMERIC fields' info on PostgreSQL provider (santi)
 - Fixed circular calls in gda_data_model_describe_column (rodrigo)
 - Added man page for configuration tool (gonzalo)
 - Added build requirements for gda-sqlite and man page for gda-config-tool
   in SPEC file (david)
 - Fixed FreeTDS include path for RedHat (holger)
 - Made all plugins resident (rodrigo)
 - Implemented basic read-onlyness for MySQL connections (rodrigo)
 - Fixed AGGREGATES schema for MySQL (rodrigo)
 - Lots of work on the reporting engine (santi)
 - Added support for PostgreSQL's bytea type fields (gonzalo)
 - Added use of syslog for the log functions (rodrigo)
 - Fixed unref'ing problems on providers (gonzalo)
 - Added first working version of the LDAP provider (german)
 - Removed obsolete config.h files all over (gonzalo)
 - Added event notification framework to GdaClient, and converted error notification
   to the new system (rodrigo)
 - Implemented saving data models to XML (rodrigo)
 - First basic version of the xBase provider (rodrigo)
 - First GNOME 2 version of the ODBC provider (nick)
 - Added support for SSL in MySQL connections (rodrigo)
 - Added MONEY type (rodrigo)
 - Unified parameter names for providers' connection strings (gonzalo)
 - Updated translations:
        - cs (mitr)
        - de (cneumair, chrisime)
	- es (pablodc)
	- no (kmaraas)
	- sk (stano)
	- sl (minmax)
	- sv (menthos)

libgnomedb 0.10.0
-----------------

 - Added --disable-gtk-doc argument to configure (rodrigo)
 - Fixed .server file paths (rodrigo)
 - Fixed weird size of provider list in configuration dialog (rodrigo)
 - Fixed modal dialog on 'save as..' grid's menu item and made the code
   actually ask the user if he wants to overwrite existing files (rodrigo)
 - Re-Added GnomeDbIconList from the 1.4 version (rodrigo)
 - Implemented session management (rodrigo)
 - Replaced GnomeDbSqlEditor widget with a more generic GnomeDbEditor,
   to support many languages (rodrigo)
 - Removed obsolete config.h files all over (gonzalo)
 - Fixed alignment for grid's cells (rodrigo)
 - Improved look of error and login dialogs (rodrigo)
 - Remember values between GnomeDbDsnConfigDruid's pages (rodrigo)
 - Many improvements on GnomeDbGrid (rodrigo)
 - Updated translations:
        - de (cneumair)
	- es (pablodc)
	- it (algol)
	- no (kmaraas)
	- pt_BR (evandrofg)
	- sl (minmax)
	- sv (menthos)

mergeant 0.10.0
---------------

 - Many bug fixes and improvements on the queries interface (vivien)
 - Fixed crash when providers return wrong schema data (vivien)
 - Added condition editor to query interface (vivien)
 - Added more settings to the configuration dialog (rodrigo)
 - Added StartupNotify support to .desktop files (rodrigo)
 - Implemented session management (rodrigo)
 - Added transactions-related commands to GUI (rodrigo)
 - Added icon area for status icons (rodrigo)
 - Improved menu bar (rodrigo)
 - Added MIME information for Mergeant files (rodrigo)
 - Updated documentation (vivien)
 - Added placeholder support to the SQL page (rodrigo)
 - Added online help (rodrigo)
 - Replaced most GtkCList's with GnomeDbGrid's (rodrigo)
 - Improved joins GUI (vivien)
 - HIG-ization of many dialogs and widgets (vivien, rodrigo)
 - Adapted to libgnomeprint* 2.2 (rodrigo)
 - Added logging tab to SQL page (rodrigo)
 - Updated translations:
        - cs (mitr)
        - de (cneumair)
	- fr (redfox)
	- sv (menthos)
	- vi (pablo)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.10.0/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview
* mergeant: libgda/libgnomedb and dependencies

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
