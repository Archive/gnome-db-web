<?php
require('html.php');

html_page_header('libgda/libgnomedb/mergeant 0.11.0 released');

?>
<pre>
libgda/libgnomedb/mergeant 0.11.0 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2 and MS SQL server, as well as MS Access and xBase
files and ODBC data sources.
Mergeant is an end user application that makes use of libgda and libgnomedb to
allow users to easily manage their databases.

This release is the next in a series that will result in 1.0, which is what GNOME-DB
developers are already working on. For this reason, we need users and developers
to use it and report any problem/suggestion.

Changes in this release since 0.10.0:

libgda 0.11.0
-------------

 - Improvements on Oracle provider (steve)
 - Added support for Oracle 9 (steve)
 - Improvements on IBM DB2 provider (sergey)
 - Fixed crash on weak references when destroying provider hash table (gonzalo)
 - ANSI-compliant fixes (tagoh)
 - Spec file fixes (yanko)
 - Fixed incorrect password problem in Postgres provider (david, gonzalo)
 - Fixed typos in Oracle provider strings (cneumair)
 - More work on the report engine (santi)
 - Made FreeTDS provider work with freetds-0.61 (tagoh)
 - Updated translations:
	- ca (jordim)
	- cs (mitr)
	- de (cneumair)
	- es (pablodc)
	- nl (adrighem)
	- pt_BR (gdvieira)
	- ru (dmitrym)
	- sv (menthos)
	- uk (rasta)

libgnomedb 0.11.0
-----------------

 - Marked missing strings in .keys file for translation (cneumair)
 - Added new stock icons for COMMIT and ROLLBACK (rodrigo)
 - Fixed #106121 (julio)
 - Fixed #106122 (julio)
 - Fixed #106402 (rodrigo)
 - HIG-ify the gray bar widget (apg)
 - Fixed #106402 (tagoh)
 - Spec file fixes (yanko)
 - Updated translations:
	- cs (mitr)
	- de (cneumair)
	- es (pablodc)
	- nl (adrighem)
	- pl (chyla)
	- pt (dnloreto)
	- pt_BR (gdvieira)
	- sv (menthos)
	- uk (rasta)

mergeant 0.11.0
---------------

 - Spec file fixes (yanko)
 - Fixed #105178 (rodrigo)
 - Fixed #103094 (rodrigo)
 - Use new libgnomedb stock icons (rodrigo)
 - Made settings dialog look like Nautilus's preferences (rodrigo)
 - Added missing features to GConf-oriented GtkEntry's (rodrigo)
 - Updated translations:
	- am (yacob)
	- cs (mitr)
	- es (pablodc)
	- nl (adrighem)
	- pl (chyla)
	- pt (dnloreto)
	- pt_BR (evandrofg)
	- sv (menthos)
	- uk (rasta)
	- vi (pablo)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.11.0/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview
* mergeant: libgda/libgnomedb and dependencies

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
