<?php
function get_latest_news () {
?>
<table  bgcolor="#eef0ff" cellpadding="3" cellspacing="0">

   <!-- INCLUDE NEW CONTENT BELOW THIS -->
   <tr><td><b>libgda/GNOME-DB 0.2.93 released</b></td></tr><tr><td><font size="-1" color="#444444">
   Date: 2001/Nov/27</font></td></tr><tr><td>
   A new version of libgda and GNOME-DB
   is available. Read the <a href="/docs/announce-0.2.93.php">full
   announcement</a>.
   </td></tr>
   <tr><td bgcolor="#ffffff">&nbsp;<!-- separator --></td></tr>
		     
   <tr><td><b>New #gnome-db IRC channel</b></td></tr><tr><td><font size="-1" color="#444444">
   Date: 2001/Nov/06</font></td></tr><tr><td>
   A new IRC channel has been created for us, gnome-db/libgda users and
   developers to talk "face-to-face". Please, join us at #gnome-db in
   <b>irc.gnome.org</b>
   </td></tr>
   <tr><td bgcolor="#ffffff">&nbsp;<!-- separator --></td></tr>

   <tr><td><b>libgda/GNOME-DB 0.2.92 released</b></td></tr><tr><td><font size="-1" color="#444444">
   Date: 2001/Oct/10</font></td></tr><tr><td>
   A new version of libgda and GNOME-DB
   is available. Read the <a href="/docs/announce-0.2.92.php">full
   announcement</a>.
   </td></tr>
   <tr><td bgcolor="#ffffff">&nbsp;<!-- separator --></td></tr>

   <tr><td><b>
   libgda/GNOME-DB 0.2.91 released</b></td></tr><tr><td><font size="-1" color="#444444">
   Date: 2001/Oct/04</font></td></tr><tr><td>
   A new version of libgda and GNOME-DB
   is available. Read the <a href="/docs/announce-0.2.91.php">full
   announcement</a>.
   </td></tr>
   <tr><td bgcolor="#ffffff">&nbsp;<!-- separator --></td></tr>

   <tr><td><b>
   libgda/GNOME-DB/gASQL 0.2.90 released</b></td></tr><tr><td><font size="-1" color="#444444">
   Date: 2001/Aug/13</font></td></tr><tr><td>
   A new version of libgda and GNOME-DB, with the special addition of gASQL
   is available. Read the <a href="/docs/announce-0.2.90.php">full
   announcement</a>.
   </td></tr>
   <tr><td bgcolor="#ffffff">&nbsp;<!-- separator --></td></tr>

   <tr><td><b>
   ADO.NET / GNOME-DB comparison</b></td></tr><tr><td><font size="-1" color="#444444">
   Date: 2001/Jul/10</font></td></tr><tr><td>
   Brian Jepson has written a <a href="http://www.oreillynet.com/pub/a/dotnet/2001/07/09/mono.html">nice article</a> in which it compares ADO.NET and GNOME-DB, following the discussions about the <a href="http://www.ximian.com/mono">Mono</a> project.
   </td></tr>
   <tr><td bgcolor="#ffffff">&nbsp;<!-- separator --></td></tr>

</table>

<?php
}
?>
