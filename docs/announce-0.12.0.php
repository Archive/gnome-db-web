<?php
require('html.php');

html_page_header('libgda/libgnomedb/mergeant 0.12.0 released');

?>
<pre>
libgda/libgnomedb/mergeant 0.12.0 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.
Mergeant is an end user application that makes use of libgda and libgnomedb to
allow users to easily manage their databases.

This release is the next in a series that will result in 1.0, which is what GNOME-DB
developers are already working on. For this reason, we need users and developers
to use it and report any problem/suggestion.

Changes in this release since 0.11.0:

libgda 0.12.0
-------------

 - Fixed password problem in PostgreSQL provider (david, gonzalo)
 - Fixed ODBC headers detection problem (rodrigo)
 - Fixed typo in FreeTDS CFLAGS (rodrigo)
 - Compilation fixes for Sybase provider (rodrigo)
 - Spec file fixes for RH9 (david)
 - Fixed libsql linking problems (rodrigo)
 - Fixed NULL password access in Oracle provider (fozzy)
 - Improved support for types in Oracle provider (fozzy)
 - Fixed index schema retrieval in Oracle provider (fozzy)
 - Fixed typo in MySQL library detection (bodo)
 - Fixed aggregates schema in Oracle provider (fozzy)
 - Some documentation fixes (cj)
 - Initial version of mSQL provider (danilo)
 - Fixed compilation problem in IBM DB2 provider (sergey)
 - Improved sample code in documentation (xabier)
 - Cleaned up GdaRow and GdaParameter APIs (xabier, rodrigo)
 - Improvements to the fields metadata schema queries in the
   PostgreSQL provider (vivien)
 - Updated translations:
        - cs (mitr)
        - de (cneumair)
	- es (pablodc)
        - fa (roozbeh)
	- it (marcoc)
	- nl (adrighem)
        - pt (dnloreto)
	- pt_BR (evandrofg, gdvieira)
	- sr (danilo)
	- sv (menthos)

libgnomedb 0.12.0
-----------------

 - Added new stock icons (apg)
 - HIG-ified the login dialog widget (apg)
 - Added support for GTK/libgda-only compilation (dmitrym)
 - Extended GnomeDbForm API (rodrigo)
 - Updated translations:
        - cs (mitr)
	- de (cneumair)
	- es (pablodc)
        - fa (roozbeh)
	- nl (adrighem)
	- pt (dnloreto)
	- pt_BR (gdvieira)
	- sv (menthos)

mergeant 0.12.0
---------------

 - Fixed UTF8 strings displaying (gonzalo)
 - Some documentation fixes (cj)
 - HIG-ify .desktop file (rodrigo)
 - HIG-ify preferences dialog (rodrigo)
 - Made menus to be disabled instead of hidden, as suggested in the
   HIG (rodrigo)
 - Made plugins install directory FHS compliant (frederic)
 - Updated translations:
        - ca (pablo)
	- cs (mitr)
        - fa (roozbeh)
	- fr (redfox)
	- nl (adrighem)
	- pt (dnloreto)
	- sv (menthos)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.12.0/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview
* mergeant: libgda/libgnomedb and dependencies

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
