<?php
require('html.php');

html_page_header('XML Queries Proposal');
?>
<pre>
-----------------------------

XML SPECIFICATION FOR QUERIES

-----------------------------

 _______________________
(                       )
( What it is for        )
(_______________________)

The XML queries will made it possible for the client to send queries in a
generic form, without having to know if the underlying DBMS understands SQL92,
or other languages and extensions. 

The XML query will contain an understandable version of
what the client wants to do (an XML version of sentenses like "put some values
in that table", "create a new table", "set the reading permission for this user
on that table", "give me the list of shoes for which the sale price is higher
than the average sale price of the blue shoes",...)

When the client wants to send a query, it will have to build the query, then
get a string version of it (which can be saved, etc), send it to the provider,
and then discard it. There will be a GdaXmlquery object and a lib to help in
the job of creating (,...) the query.

Each provider will then have to get the XML string of the query, build back the
corresponding GdaXmlquery object, convert it to an understandable query for
the RDBMS it represents, send the query to the RDBMS, discard the GdaXmlquery
object and return the result of the query to the client.


The advantages of such a way of proceding are:
----------------------------------------------
* no need to worry about the language specificities of the underlying RDBMS (no
more question like "do I need to format a date like MM-DD-YYYY or DD/MM/YY")
* it will be possible to save and restore in an easy way the GdaXmlquery
objects.
* I hope that in the future, the client will be able to ask the provider to
give him the XML Queries to save the structure and the data of a database (in
the way gda-xml lib does now, but with more accuracy).
* It will be possible to ask the provider for an equivalent, in the language
its RDBMS use, of the XML query to give some feedback to the user of the 
client.
* if some RDBMS have some extensions, it will be possible to test if the
provider the client is connected to supports theses extensions (with the
supports() function, like for example nested select queries), and then create
the XML query accordingly.


 _______________________
(                       )
( Detailled description )
(_______________________)

This query specification allows to ask for only the SELECT, DELETE, INSERT and
UPDATE operations for now, but has the possibility to be extended for CREATE
TABLES, CREATE FUNCTIONS, etc.

See at the end for a DTD proposal for validating the XML files following this 
description.

A query is specified under the &lt;QUERY&gt; tag, and contains several subtrees:
* a TARGET TREE, tagged &lt;TARGET&gt;, that lists the tables or views that are 
  modified (or created) by the query (the user need to have a write permission
  to avery of these listed elements). Not all queries need to have that tree.
* a SOURCE TREE, tagged &lt;SOURCES&gt;, that lists the affected tables by the query,
  the ones where the query will have to look to get information (the user need
  to have a read access to ALL these tables). Not all queries need to have
  that tree.
* a VALUES TREE, tagged &lt;VALUES&gt;, which lists all the detailled parts of the
  query. For a SELECT query, these are the elements found after the SELECT. For
  an INSERT or UPDATE query, these are the elements specifying what gets 
  inserted or updated and their respective values.
* a QUAL TREE, tagged &lt;QUALIFICATION&gt;, which is the list of rules to qualify
  an entry in a table to submit it or not to the query (usually what appears
  after WHERE or HAVING clauses)

The &lt;QUERY&gt; tag has a mandatory property which is the kind of operation it
represents. It is the "op" property and can be either "SELECT", "INSERT",
"UPDATE" or "DELETE".

Here is a detailled description of each tag and the possible included sub tags:

&lt;TARGET&gt;
--------
The &lt;TARGET&gt; and &lt;SOURCES&gt; trees are fairly simple.
That subtree can contain a table or view name. If a table 
is in the list, it can have an optional property named "temp" which can be set
to "yes" if the table will be created by the query and needs not to exist when
the session is closed (this is an optionnal feature).

For example:
&lt;TARGET&gt;
   &lt;TABLE&gt;mytable&lt;/TABLE&gt;
   or
   &lt;TABLE temp="yes"&gt;mytable&lt;/TABLE&gt;
   or
   &lt;VIEW&gt;myview&lt;/VIEW&gt;
&lt;/TARGET&gt;


&lt;SOURCES&gt;
--------
That subtree can contain the same information as the TARGET one except for some
optionnal values. the &lt;TABLE&gt; and &lt;VIEW&gt; tags can have one optionnal property
named "alias", and one optionnal property named "id" (which must be unique to 
all such IDs) in the file. 

For example:
&lt;SOURCES&gt;
   &lt;TABLE id="table01"&gt;mytable&lt;/TABLE&gt;
   &lt;VIEW alias="v"&gt;myview&lt;/VIEW&gt;
&lt;/SOURCES&gt; 


&lt;VALUES&gt;
--------
This tree is a bit more complex. It can contain 4 types of tags which are
explained below:
** the &lt;CONST&gt; tag is used to specify a constant value and can have a property
   named "printname". For example:
   &lt;CONST printname="Length"&gt;150&lt;/CONST&gt;

** the &lt;QUERY&gt; tag is there to specify a SELECT query (can be usefull in the
   INSERT operation to insert into a table the result of another query)

** the &lt;FIELD&gt; tag will be the most often used, it specifies a table or view's
   field (and can give a value to it, depending on the kind of query, like in
   INSERT queries). The field needs to refer to a table or a view (which is
   done by setting to the table or view's ID the mandatory attribute "source".
   The name of the field is given with the mandatory property "name".
   There are other optionnal properties which are "printname", and "group" 
   (set to "yes" if the query will have to use this field to create groups, the
   GROUP BY clause)
   Here is an example, 150 is the value of the "myfield" field:
   &lt;FIELD source="table01" name="myfield" printname="My Label"&gt;
      150
   &lt;/FIELD&gt;

** the &lt;FUNC&gt; tag is the most complex one. It allows to use functions, and is
   recursive (can contain other &lt;FUNC&gt;, &lt;CONST&gt; or &lt;FIELD&gt; tags) for complex
   functions. It has a mandatory property "name" which is the name of the
   function to call and an optionnal "printname" property.
   Here is an example:
   &lt;FUNC printname="My Function Result" name="myfunc"&gt;
      &lt;FIELD source="table01" name=myfield"/&gt;
      &lt;CONST&gt;150&lt;/CONST&gt;
   &lt;/FUNC&gt;
   will result in: myfunc(mytable.myfield, 150)


&lt;QUALIFICATION&gt;
---------------
This subtree contains all qualification information. It is what we can find
just after the WHERE or HAVING clauses. The distinction between WHERE and
HAVING clauses will be made by the provider part.

Inside this subtree there can be the following subtrees:
** the &lt;AND&gt;, &lt;OR&gt;, &lt;NOT&gt;, etc to logically articulate the conditions.
   If two conditions are found without one of these tag to articulate them,
   then an AND will be assumed.
** the &lt;EQUAL&gt;, &lt;INF&gt;, &lt;INFEQ&gt;, &lt;SUP&gt;, &lt;NONQUAL&gt;, &lt;LIKE&gt;, &lt;CONTAINS&gt;, etc will
   contain the actual qualification clauses.

Here is a small example to figure how it works. Suppoe we want to state the
following conditions: 
"WHERE size&gt;(SELECT AVG(size) from shoes) AND (color='blue' OR color='red')"
it would translate into:
&lt;QUALIFICATION&gt;
   &lt;SUP&gt;
      &lt;FIELD source="t01" name="size"/&gt;
      &lt;QUERY op="SELECT"&gt;
         &lt;SOURCES&gt;
	    &lt;TABLE id="t01"&gt;shoes&lt;/TABLE&gt;
	 &lt;/SOURCES&gt;
	 &lt;VALUES&gt;
	    &lt;FUNC name="avg"&gt;
	       &lt;FIELD source="t01" name="size"/&gt;
	    &lt;/FUNC&gt;   
	 &lt;/VALUES&gt;
      &lt;/QUERY&gt;
   &lt;/SUP&gt;
   &lt;OR&gt;
      &lt;EQUAL&gt;
         &lt;FIELD source="t01" name="color"/&gt;
	 &lt;CONST&gt;'blue'&lt;/CONST&gt;
      &lt;/EQUAL&gt;
      &lt;EQUAL&gt;
         &lt;FIELD source="t01" name="color"/&gt;
	 &lt;CONST&gt;'red'&lt;/CONST&gt;
      &lt;/EQUAL&gt;
   &lt;/OR&gt;
&lt;/QUALIFICATION&gt;
         
It seems to be complicated, but it is not really, once you understand the
structure. 


 _____
(     )
( DTD )
(_____)
&lt;!ELEMENT QUERY (TARGET?, SOURCES?, VALUES, QUALIFICATION?)&gt;
&lt;!ATTLIST QUERY 
          op(SELECT|INSERT|UPDATE|DELETE) #REQUIRED&gt;

&lt;!ELEMENT TARGET (TABLE|VIEW)*&gt;

&lt;!ELEMENT SOURCES (TABLE|VIEW)*&gt;

&lt;!ELEMENT VALUES (CONST|QUERY|FIELD|FUNC)+&gt;

&lt;!ELEMENT QUALIFICATION (AND|OR|NOT|EQUAL|NONEQUAL|INF|INFEQ|SUP|SUPEQ|NULL|LIKE|CONTAINS)*&gt;



&lt;!ELEMENT TABLE (#PCDATA)&gt;
&lt;!ATTLIST TABLE 
          id ID #IMPLIED
          temp(yes|no) #IMPLIED
          alias CDATA  #IMPLIED&gt;

&lt;!ELEMENT VIEW (#PCDATA)&gt;
&lt;!ATTLIST VIEW
          id ID #IMPLIED
          alias CDATA #IMPLIED&gt;

&lt;!ELEMENT CONST (#PCDATA)&gt;
&lt;!ATTLIST CONST
          printname #IMPLIED&gt;

&lt;!ELEMENT FIELD (#PCDATA)&gt;
&lt;!ATTLIST FIELD
          source IDREF #REQUIRED
          name CDATA #REQUIRED
          printname #IMPLIED
          group(yes|no) #IMPLIED&gt;

&lt;!ELEMENT FUNC (FIELD|CONST|FUNC)*&gt;
&lt;!ATTLIST FUNC 
          name CDATA #REQUIRED
          printname #IMPLIED&gt;

&lt;!ELEMENT AND (AND|OR|NOT|EQUAL|NONEQUAL|INF|INFEQ|SUP|SUPEQ|NULL|LIKE|CONTAINS)+&gt;
&lt;!ELEMENT OR (AND|OR|NOT|EQUAL|NONEQUAL|INF|INFEQ|SUP|SUPEQ|NULL|LIKE|CONTAINS)+&gt;
&lt;!ELEMENT NOT (AND|OR|NOT|EQUAL|NONEQUAL|INF|INFEQ|SUP|SUPEQ|NULL|LIKE|CONTAINS)&gt;
&lt;!ELEMENT EQUAL ((CONST|FIELD|FUNC),(CONST|FIELD|FUNC))&gt;
&lt;!ELEMENT NONEQUAL ((CONST|FIELD|FUNC),(CONST|FIELD|FUNC))&gt;
&lt;!ELEMENT INF ((CONST|FIELD|FUNC),(CONST|FIELD|FUNC))&gt;
&lt;!ELEMENT INFEQ ((CONST|FIELD|FUNC),(CONST|FIELD|FUNC))&gt;
&lt;!ELEMENT SUP ((CONST|FIELD|FUNC),(CONST|FIELD|FUNC))&gt;
&lt;!ELEMENT SUPEQ ((CONST|FIELD|FUNC),(CONST|FIELD|FUNC))&gt;
&lt;!ELEMENT NULL (CONST|FIELD|FUNC)&gt;
&lt;!ELEMENT LIKE ((CONST|FIELD|FUNC),(CONST|FIELD|FUNC))&gt;
&lt;!ELEMENT CONTAINS ((CONST|FIELD|FUNC),(CONST|FIELD|FUNC))&gt;



 ________________________
(                        )
( Implementation details )
(________________________)

FIXME
</pre>
<?php
html_page_footer();

?>
