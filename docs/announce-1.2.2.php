<?php
require('html.php');

html_page_header('libgda/libgnomedb 1.2.2 released');

?>
<pre>
libgda/libgnomedb 1.2.2 have been released.

libgda/libgnomedb are a complete framewok for developing
database-oriented applications, and currently allow access to PostgreSQL,
MySQL, Oracle, Sybase, SQLite, FireBird/Interbase, IBM DB2, mSQL and MS
SQL server, as well as MS Access and xBase files and ODBC data sources.
                                                                                
libgda/libgnomedb are the base of the database support in the GNOME
Office application suite, providing database access for many features in
both Gnumeric and Abiword.

Changes since libgda 1.2.1
--------------------------
 - Fixed typo in GdaXqlUpdate object declaration (Murray)
 - GCC 4 compilation fixes (Murray, Magnus)
 - Made Oracle provider work with Oracle 10 headers (Magnus)
 - Fixed MySQL header/libs detection (Jean)
 - Fixed typo in GdaExport class signal creation (Mike Kestner)
 - Set correctly unsigned values in MySQL provider (Mike Fish)
 - Fixed leak in PostgreSQL provider (Alex)

Changes since libgnomedb 1.2.1
------------------------------
 - Fixed typo in GnomeDbList class signal creation (Mike Kestner)

Tarballs are available at
http://download.gnome.org/pub/GNOME/sources/libgda/1.2/
http://download.gnome.org/pub/GNOME/sources/libgnomedb/1.2/
                                                                                
To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade,
libbonoboui and, optionally, gtksourceview
                                                                                
You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything
you want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
