<?php
require_once('lib/XPath.class.php');
require_once('content-admin/Content.php');

/**
 * Class for creating, modifying or printing news
 */
class News extends Content{

    /** Reference to the news content */
    var $news;

    /**
     * Class constructor
     */
    function News() {
	  /*$newsfile = "/var/www/www.gnome-db.org/xml-content/news.xml";*/
	  $newsfile = $this->XML_CONTENT_DIR."/news.xml";
	  $this->$news = new XPath($newsfile);
    }

    /**
     * Iterates the last news printing them.
     *
     * @param $newsToRead Number of news to print.
     * @param $detailed If true the longbody is printed. Default is 'false'
     */
    function print_latest($newsToRead = 1000, $detailed = false) {
	  $results = $this->$news->evaluate("/newscollection[1]/newsitem");
	  $newsRead = 0;
	  foreach ($results as $result) {
		$newsRead++;
		if ($newsRead > $newsToRead) {break;}
		$title = html_decode($this->$news->substringData($result."/title[1]"));
		$date = html_decode($this->$news->substringData($result."/date[1]"));
		$shortbody = html_decode($this->$news->substringData($result."/shortbody[1]"));
		$longbody = html_decode($this->$news->substringData($result."/longbody[1]"));
		
		$newsId = $this->$news->getAttributes($result, "id");
		$this->print_item($title, $date, $shortbody,
		                  $longbody, $detailed, $newsId);
	  }
    }

    /**
     * Adds a news item (Not tested yet).
     */
    function add_item($title, $shortBody, $longBody) {
	  $newsfile = "/var/www/www.gnome-db.org/xml/news.xml";
	  	  
	  $date = date("d M Y");
	  $newsId = date("YmdHis");
	  
	  $this->$news->appendChild("/newscollection[1]","newsitem");
	  $this->$news->appendChild("/newscollection[1]/newsitem[1]","title");
	  $this->$news->replaceData("/newscollection[1]/newsitem[1]/title[1]", $title);
	  $this->$news->appendChild("/newscollection[1]/newsitem[1]","date");
	  $this->$news->replaceData("/newscollection[1]/newsitem[1]/date[1]", $date);
	  $this->$news->appendChild("/newscollection[1]/newsitem[1]","shortbody");
	  $this->$news->replaceData("/newscollection[1]/newsitem[1]/shortbody[1]", $shortBody);
	  $this->$news->appendChild("/newscollection[1]/newsitem[1]","longbody");
	  $this->$news->replaceData("/newscollection[1]/newsitem[1]/longbody[1]", $longBody);
//	  $this->$news->save_to_file("$newsfile");
	  $this->$news->exportToFile("$newsfile");
    }
	

	/**
	 * Print a news item using HTML formatting
	 */
    function print_item($title, $date, $shortbody, $longbody,
                        $detailed, $newsId="0") {
    ?>
    <table width="100%"  bgcolor="#eef0ff" cellpadding="3" cellspacing="0">
       <tr id="<?php print 'D'.$newsId; ?>"><td><a name="<?php print 'D'.$newsId; ?>"></a><b>
	     <?php echo $title;?>
       </b></td></tr><tr><td><font size="-1" color="#444444">
		 <?php echo $date; ?>
       </font></td></tr>
       <tr><td>
	     <?php echo str_replace ("&", "&amp;", $shortbody); ?>
	     <p>
	       <?php 
			if ($detailed) {
			  echo str_replace ("&", "&amp;", $longbody); 
			} else {
			  if ($longbody != "") {
				echo "<a href='news.php#D".$newsId."'>More...</a>";
			  }
			}
		   ?>
		 </p>
	   </td></tr>
       <tr><td bgcolor="#ffffff">&nbsp;<!-- separator --></td></tr>
    </table>
    <?php
    }
}

?>
