<?php
require('html.php');

html_page_header('GNOME-DB 0.2.0 released');

?>
<pre>
GNOME-DB 0.2.0 (a.k.a. DB components) has been released.

GNOME-DB is a complete framewok for developing database-oriented
applications, and actually allows access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
it also includes several applications and utilities which make it
quite suitable for many database-related taks.

This new release includes:
* clean separation between the GNOME and non-GNOME part: libgda, the data
  access framework, is now 100% GNOME-independent, and is almost ready to
  make the move from GTK object system to GLib's GObject as soon as
  GLib 1.3/2.0 is released
* componentized architecture: the GNOME part has been split into several
  Bonobo components to allow their reuse in other Bonobo applications. Now,
  the GNOME-DB Front End application is just a Bonobo container which loads
  the different GNOME-DB components: SQL, Config manager, DB designer, log
  viewer, etc
* new Sybase provider: support for accessing Sybase databases has been added
* better look&feel: GNOME-DB now makes use of libgal, the GNOME Applications
  Library, which gives a better look to the user interface
* new web site and domain: the gnome-db.org domain was registered this week and
  thus now the new web site is at http://www.gnome-db.org. This new site will
  serve as the meeting point for people developing applications based on
  GNOME-DB/libgda

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf >= 0.9, libxml
* gnome-db: libgda and dependencies, Bonobo >= 0.28, gnome-libs, libgdal, gtkhtml

The GNOME-DB team is working on the following to bring it to your desktops ASAP:
* TDS provider: this provider will be making use of the FreeTDS (http://www.freetds.org),
  which is a free implementation of the TDS protocol, which will allow access
  to both Sybase and Microsoft SQL Server databases
* MDB provider: this provider, which uses the libmdb library developed by the
  MDBTools project (http://mdbtools.sourceforge.net), will give access to
  Microsoft Access MDB files
* report engine: a CORBA-based report engine is being developed to allow the
  generation and storage of database-based reports
* integration of all GNOME-DB widgets into Glade

You can download the package and find other sources of information
in http://www.gnome-db.org.
</pre>
<?php

html_page_footer('');

?>
