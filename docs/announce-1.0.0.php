<?php
require('html.php');

html_page_header('libgda/libgnomedb 1.0.0 released');

?>
<pre>
libgda/libgnomedb 1.0.0 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.

libgda/libgnomedb are the base of the database support in the GNOME Office
application suite, providing database access for many features in both
Gnumeric and Abiword.

This release is almost identical to 0.99.0, aka RC1.

libgda 1.0.0
------------

 - Updated API documentation (rodrigo)

libgnomedb 1.0.0
----------------

 - Fixed #122108 - generating .server is not flexible (tagoh)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v1.0.0/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
