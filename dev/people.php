<?php
require('html.php');

html_page_header('People Working on GNOME-DB','people');
?>
	<p>
		<b>GNOME-DB</b>is possible thanks to the help and collaboration of many
		people. This is the list of people who have contributed in some way to
		<b>GNOME-DB</b>.
		<ul>
			<li><a href="mailto:michael@lausch.at">Michael Lausch</a>, Founder of the project</li>
			<li><a href="mailto:rodrigo@gnome-db.org">Rodrigo Moya</a>, Main programmer and maintainer</li>
			<li><a href="mailto:stephan.heinze@xcom.de">Stephan Heinze</a>, Oracle provider</li>
			<li><a href="mailto:malerba@gnome-db.org">Vivien Malerba</a>, PostgreSQL provider</li>
			<li><a href="mailto:nick@lurcher.org">Nick Gorham</a>, ODBC provider</li>
			<li><a href="mailto:cwiegand@urgentmail.com">Chris Wiegand</a>, C++ bindings</li>
			<li><a href="mailto:acs@barrapunto.com">Alvaro del Castillo</a>, Rolodex application</li>
			<li><a href="mailto:tagoh@gnome.gr.jp">Akira TAGOH</a>, Debian packages</li>
			<li><a href="mailto:carlos@gnome-db.org">Carlos Perell&oacute; Mar&iacute;n</a>, libSQLite provider, Report engine</li>
      <li><a href="mailto:reinhard@gnue.org">Reinhard M�ller</a>, GLib 1.3/2.0 integration</li>
			<li><a href="mailto:alvaro@linuxfan.com">Alvaro L&oacute;pez</a>, Python bindings</li>
      <li><a href="mailto:holger.thon@gnome-db.org">Holger Thon</a>, Sybase and TDS providers</li>
      <li><a href="mailto:dj@starfire-programming.net">Danilo Sch&ouml;neberg</a>, mSQL provider</li>
			<li><a href="mailto:e98cuenc@yahoo.com">Joaqu&iacute;n Cuenca</a>, UI improvements</li>
      <li><a href="mailto:jferrer@ieeesb.etsit.upm.es">Jorge Ferrer</a>, Webmaster</li>
			<li><a href="mailto:gonzalo AT gnome-db .org">Gonzalo Paniagua Javier</a>, GNOME 2 PostgreSQL provider, libgda</li>
			<li><a href="mailto:cas AT starfire-programming .org">Chris Silles</a>, PHP/gda Module, libgda</li>
			<li><a href="mailto:lrz@gnome.org">Laurent Sansonetti</a>, Ruby bindings, libgda</li>
			<li>
				Translators:
				<ul>
					<li><a href="mailto:myv@iki.fi">Markku Verkkoniemi</a>, Finnish po translator</li>
					<li><a href="mailto:menthos@menthos.com">Christian Rose</a>, Swedish po translator</li>
					<li><a href="mailto:olea@hispalinux.es">Ismael Olea</a>, Spanish po translator</li>
					<li><a href="mailto:barreiro@arrakis.es">Manuel de Vega Barreiro</a>, Spanish po translator</li>
				</ul>
			</li>
		</ul>
	<p>
		Development chief is <a href="mailto:rodrigo@gnome-db.org">Rodrigo Moya</a>, so contact
		him if you're planning to help in some way the <b>GNOME-DB</b> project.
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
