<?php
require('html.php');

html_page_header('GNOME-DB/libgda/gASQL 0.2.90 released');

?>
<pre>
GNOME-DB/libGDA/gASQL 0.2.90 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
they also include several applications and utilities which make them
quite suitable for many database-related taks.
gASQL is a database administration tool based on libgda and GNOME-DB, featuring
visual tools for managing any database supported by libgda/GNOME-DB.

This release marks a big architectural change, which makes the whole
suite much more stable and now 99% ready for use by everyone

Changes:

	* libgda is now based in Bonobo, meaning that we don't have to make
	  any more hacks in GNOME-DB to provide GDA features through the
	  Bonobo infrastructure, but that we can do it directly from
	  libgda. Some IDL changes have been made to better integrate
	  the GDA CORBA interfaces with the Bonobo ones, reusing for this
	  the interfaces provided by Bonobo for many different tasks. This
	  integration with Bonobo will also make it easier when we migrate
	  the whole suite to GLib 2.0 (Rodrigo)

	* Fixes in the MySQL provider: lots of stuff have been fixed for
	  the MySQL provider, as well as some missing functionality
	  added. The MySQL provider is now (almost) as good as the PostgreSQL
	  one (Vivien)

	* Added clipboard's copy support to the GnomeDbGrid widget, so now
	  you've got another way of sharing the data retrieved with GNOME-DB /
	  libgda and other unrelated applications (Rodrigo)

	* UI changes: Bonobo components are now using the XML based Bonobo UI
	  feature, thus having access to lots of new features in the
	  UI merging of the different components that compose GNOME-DB (Rodrigo)

	* API changes: some small changes were introduced in the libgda API,
	  mainly to add new utility functions built on top of the already
	  existing infrastructure (Rodrigo)

	* Improved Oracle provider: although not much new features have been
	  added, it shoud be much more stable now (Rodrigo)

	* Support for viewing more objects in the database browser: apart
	  from the already supported object types (tables, views, procedures and
	  types), we've added support for viewing aggregate functions and
	  sequences (Rodrigo)

	* Export databases: basic support has been added to allow the exporting
	  of databases. Right now, only tables are exported (Rodrigo)

	* Perl bindings: we've got now an initial version of the libgda
	  Perl bindings, which will allow people to use libgda from Perl, as
	  well as the addition, to the front end, of scripting support (Gregor)

	* Updated API reference: although still missing a lot, our API
	  reference is getting better (Rodrigo)

	* Sample source code: 2 new small sample programs have been added to
	  GNOME-DB sources, which show how easily you can use libgda/gnome-db
	  in your applications (Brian)

        * Starting from this version, gASQL will be released along with 
	  GNOME-DB/libGDA. For more information on gASQL, please refer to
	  http://gasql.sourceforge.net (Vivien)

	* Updated translations (GNOME Translation Team)

Bugs fixed:

	* adapted the Oracle provider to the newest Oracle 8i and Oracle
	  9i (Bob)

	* fixed the DSN wizard, which was not building the connection string
	  correctly (Rodrigo)

	* first-attempt-to-connect bug fixed (Bonobo :-)

	* GnomeDbCombo is not broken anymore (Rodrigo)

Now in development:

	* Perl scripting support in the front-end
	* CORBA-based report engine

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml1, Bonobo >= 1.0.x
* gnome-db: libgda and dependencies, gnome-libs

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
