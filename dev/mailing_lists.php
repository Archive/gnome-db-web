<?php
require('html.php');

html_page_header('Mailing Lists','mailinglists');
?>
	<p>
		There is a mailing list for general discussion about GNOME-DB/libgda
		issues thar you should subscribe to if you're going to use or help
		in the development of GNOME-DB/libgda. You can subscribe to that list
		directly from your <a href="http://mail.gnome.org/mailman/listinfo/gnome-db-list">browser</a>.
	</p>
	<p>
		This is list is <a href="http://mail.gnome.org/archives/gnome-db-list/index.html">archived</a>,
		so you'll never miss a message.
	</p>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
