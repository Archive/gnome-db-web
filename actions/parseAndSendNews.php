<?php
require('html.php');
html_page_header('Thanks!','news');
$formatedDate = date("Y/M/d");
?>

<p>
<font size="+1" color="#333355">

Your contribution has been sent and will be reviewed.</font></p>
<?php
$news = new News();
$strippedTitle=stripslashes($newsTitle);
$strippedShortbody=stripslashes($shortbody);
$strippedLongbody=stripslashes($longbody);
$news->print_item($strippedTitle, $formatedDate, $strippedShortbody, 
                  $strippedLongbody, true);
html_page_footer();
?>

<?php

//
// SENDING THE E-MAIL

$from = "webmaster@gnome-db.org";
//$destinatary = "webmaster@gnome-db.org";
$destinatary = "jfz@infonegocio.com";
$aboutString = "Content for the gnome-db web";
$encodedTitle = htmlentities(stripslashes($newsTitle));
$encodedShortbody = htmlspecialchars(stripslashes($shortbody));
$encodedLongbody = htmlspecialchars(stripslashes($longbody));
$newsId = date("YmdHis");

$messageString = "
This message has content which should be included in
the gnome-db web page. The content is:

TITLE: $strippedTitle
SHORT BODY:
$strippedShortbody
LONG BODY:
$strippedLongbody

To include it in our pages you have to follow this steps:
1) Obtain the latest version of news.xml from CVS
   (cvs co gnome-db/www/include/xml-content/news.xml)
2) Add the following XML code as the first child inside the
   'newscollection' element 

   <newsitem id='$newsId'>
     <title>$encodedTitle</title>
     <date>$formatedDate</date>
     <shortbody>$encodedShortbody</shortbody>
     <longbody>$encodedLongbody</longbody>
   </newsitem>

3) Commit the changes to cvs (cvs ci news.xml from its directory)
";

n_mail($from, $destinatary, $aboutString, $messageString, "");

?>