<?php
require('html.php');

html_page_header('Send a new to the GNOME-DB team',"news");
$news = new News();
?>

<form action="actions/parseAndSendNews.php" enctype="text/plain" name="f">
<table cellspacing="10">
<tr>
  <td align="right" valign="top">
  <b>Title</b></td>
  <td>
  <input name="newsTitle" type="text" size="40"></td>
</tr>
<tr>
  <td align="right" valign="top">
  <b>Short body</b></td>
  <td>
  <textarea name="shortbody" type="text" cols="30" rows="5"></textarea></td>
</tr>
<tr>
  <td align="right" valign="top">
  <b>Long body</b></td>
  <td>
  <textarea name="longbody" type="text" cols="30" rows="5"></textarea></td>
</tr>
<tr>
  <td></td>
  <td>
  <input type="submit" value=" Send "></td>
</tr>

<tr>
  <td></td>
  <td>
    <h1>Previous news</h1></td>
<tr>
  <td></td>
  <td>
<?php
   $news->print_latest(15);
?>
  </td>
</tr>
</table>
</form>

<?php
$lastModifiedTime = filemtime('addnews.php');
html_page_footer($lastModifiedTime);

?>






