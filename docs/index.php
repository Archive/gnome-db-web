<?php
require('html.php');

html_page_header('Documentation','documentation');
?>
	<p>
		In these pages you'll find all documentation currently available for GNOME-DB
		and libgda. If you write something about them, ot if you know of a site where
		there is something said about GNOME-DB, please
		<a href="mailto:webmaster@gnome-db.org">tell us</a> and we'll include it here.
	<h3>Manuals</h3>
	<p>
		The links below will get you to the GNOME-DB, libgnomedb and libgda manuals:
		<ul>
			<li>
				libgda manual:
				<a href="libgda/index.html">HTML online</a>
			</li>
			<li>
				libgnomedb manual:
				<a href="libgnomedb/index.html">HTML online</a>
			</li>
			<li>
				Mergeant manual (new version):
				<a href="mergeant/index.html">HTML online</a>
			</li>
		</ul>
	<h3>White Papers</h3>
	<p>
		These are short documents usually made by a GNOME-DB/libgda developer as
		a proposal for the addition or development of some part of the
		architecture. The available ones are:
		<ul>
			<li>
				<a href="white-papers/xml-queries.php">XML Queries proposal</a>, by
				<a href="mailto:malerba@gnome-db.org">Vivien Malerba</a>
			</li>
		</ul>
	<h3>Documentation in other languages</h3>
	<p>
		Thanks to the work of the different GNOME translations teams, we happen to
		have GNOME-DB/libgda documentation in some languages other than English.
		These are:
		<ul>
			<li><a href="es/">Spanish</a></li>
			<li><a href="it/">Italian</a></li>
			<li><a href="pt_BR/">Brazilian Portuguese</a></li>
		</ul>
	<h3>Release announcements</h3>
	<p>
		Here you'll find the announces related to each GNOME-DB/libgda release:
		<li><b>libgda/libgnomedb</b></li>
		<ul>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-August/msg00007.html">1.3.90 is the latest unstable release</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-July/msg00010.html">1.3.4</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-June/msg00003.html">1.3.3</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-June/msg00001.html">1.3.2</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-March/msg00039.html">1.3.1</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-February/msg00026.html">1.3.0</a></li>
			<li><a href="announce-1.2.2.php">1.2.2 is the latest stable release</a></li>
			<li><a href="announce-1.2.1.php">1.2.1</a></li>
			<li><a href="announce-1.2.0.php">1.2.0</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2004-August/msg00030.html">1.1.6</a></li>
			<li><a href="announce-1.0.4.php">1.0.4</a></li>
			<li><a href="announce-1.0.3.php">1.0.3</a></li>
			<li><a href="announce-1.0.2.php">1.0.2</a></li>
			<li><a href="announce-1.0.1.php">1.0.1</a></li>
			<li><a href="announce-1.0.0.php">1.0.0</a></li>
			<li><a href="announce-0.99.0.php">0.99.0</a></li>
			<li><a href="announce-0.91.0.php">0.91.0</a></li>
			<li><a href="announce-0.90.0.php">0.90.0</a></li>
		</ul>
		<li><b>Mergeant</b></li>
		<ul>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-August/msg00008.html">0.62 is the latest version</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-July/msg00018.html">0.61</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2005-June/msg00002.html">0.60</a></li>
			<li><a href="http://mail.gnome.org/archives/gnome-db-list/2004-August/msg00031.html">0.52</a></li>
			<li><a href="announce-0.12.1.php">0.12.1</a></li>
		</ul>
		<li><b>Common releases (old)</b></li>
		<ul>
			<li><a href="announce-0.12.1.php">0.12.1</a></li>
			<li><a href="announce-0.12.0.php">0.12.0</a></li>
			<li><a href="announce-0.11.0.php">0.11.0</a></li>
			<li><a href="announce-0.10.0.php">0.10.0</a></li>
			<li><a href="announce-0.9.0.php">0.9.0</a></li>
			<li><a href="announce-0.8.199.php">0.8.199</a></li>
			<li><a href="announce-0.8.193.php">0.8.193</a></li>
			<li><a href="announce-0.8.192.php">0.8.192</a></li>
			<li><a href="announce-0.8.191.php">0.8.191</a></li>
			<li><a href="announce-0.8.190.php">0.8.190</a></li>
			<li><a href="announce-0.8.105.php">0.8.105</a></li>
			<li><a href="announce-0.2.96.php">0.2.96</a></li>
			<li><a href="announce-0.2.95.php">0.2.95</a></li>
			<li><a href="announce-0.2.94.php">0.2.94</a></li>
			<li><a href="announce-0.2.93.php">0.2.93</a></li>
			<li><a href="announce-0.2.92.php">0.2.92</a></li>
			<li><a href="announce-0.2.91.php">0.2.91</a></li>
			<li><a href="announce-0.2.90.php">0.2.90</a></li>
			<li><a href="announce-0.2.10.php">0.2.10</a></li>
			<li><a href="announce-0.2.9.php">0.2.9</a></li>
			<li><a href="announce-0.2.3.php">0.2.3</a></li>
			<li><a href="announce-0.2.2.php">0.2.2</a></li>
			<li><a href="announce-0.2.1.php">0.2.1</a></li>
			<li><a href="announce-0.2.0.php">0.2.0</a></li>
			<li><a href="announce-0.1.0.php">0.1.0</a></li>
			<li><a href="announce-0.0.96.php">0.0.96</a></li>
			<li><a href="announce-0.0.95.php">0.0.95</a></li>
		</ul>
	<h3>Other documentation</h3>
	<p>
		Here are listed other documents/resources about
		GNOME-DB/libgda found in other places around the net:
		<ul>
			<li><a href="http://www.oreillynet.com/pub/a/dotnet/2001/07/09/mono.html">Mono unveiled</a>, an article by <a href="http://www.oreillynet.com/pub/au/125">Brian Jepson</a>, in which a comparison between ADO.NET and GNOME-DB is made</li>
			<li>
				<a href="ftp://kalamazoolinux.org/pub/pdf/dbaccess.pdf">Data
				Access with Linux</a>: a presentation made by
				<a href="mailto:adam@morrison-ind.com">Adam
				Tauno Williams</a>, where several tools and
				libraries for database access are presented,
				including, of course, GNOME-DB and libgda.
			</li>
			<li>
				<a href="http://www.es.gnome.org/documentacion/charlas/gnome-db.html">GNOME-DB talk</a>
				(in Spanish), presented by
				<a href="mailto:rodrigo@gnome-db.org">Rodrigo
				Moya</a> for the III
				<a href="http://congreso.hispalinux.es">Hispalinux
				Congress</a>.
			</li>
		</ul>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
