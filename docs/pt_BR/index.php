<?php
require('html.php');

html_page_header('Documenta&ccedil;&atilde;o');
?>
	<p>
		Aqui voc&ecirc; vai encontrar a documenta&ccedil;&atilde;o oficial do Libgda e Gnome-DB dispon&iacute;vel em Portugu&ecirc;s (do Brasil). Se voc&ecirc; tiver adi&ccedil;&otilde;es a fazer, ou seja, tem alguma documenta&ccedil;&atilde; (seja um curto artigo, tutorial ou coisa do tipo) escrita sobre Libgda e/ou Gnome-DB e gostaria de diponibiliz&aacute;-los nesta p&aacute;gina, escreva para <a href="mailto:cleber@gnome-db.org">cleber@gnome-db.org</a> para maiores informa&ccedil;&otilde;es.
	</p>
	<h3>Manuais</h3>
	<p>
		A documenta&ccedil;&atilde;o est&aacute; listada abaixo, com links para diferentes formatos:
		<ul>
			<li>
				Manual da Libgda:
				<a href="manual/libgda/index.html">HTML online</a>,
				<a href="manual/libgda-manual-html.tar.gz">HTML tarball</a>,
				<a href="manual/libgda-manual.pdf">PDF</a>,
				<a href="manual/libgda-manual.ps">PostScript</a>
			</li>
			<li>
				Manual do Gnome-DB:
				<a href="manual/gnome-db/index.html">HTML online</a>,
				<a href="manual/gnome-db-manual-html.tar.gz">HTML tarball</a>,
				<a href="manual/gnome-db-manual.pdf">PDF</a>,
				<a href="manual/gnome-db-manual.ps">PostScript</a>
			</li>
		</ul>
	</p>
<?php
html_page_footer("May 4th, 2001");

?>
