<?php
require('html.php');

html_page_header('Applications using GNOME-DB/libgda','applications');
?>
	<p>
		As libgnomedb/libgda evolve to a professional database access suite, more
		applications start to use its power for more specific needs. Here
		is a list of applications doing so, and their home page. If you know
		of another application that may be included here, or if you try to
		develop something by basing your code on GNOME-DB, please
		<a href="mailto:webmaster@gnome-db.org">contact us</a>, so that it is
		included on this list.
	<ul>
		<li>
			<a href="http://www.abisource.com">AbiWord</a>, the free word
			processing application initially developed by AbiSource, uses
			libgda and libgnomedb to add database integration, such as
			mail merging.
		</li>
		<li>
			<a href="http://papyrus.treshna.com">Papyrus</a> is an XML reporting engine
			 for linux.  It enables you to generate reports from a variety of different
			 SQL databases. Your reports can be generated as PDF, PostScript, XML, HTML,
			  DVI, Latex or ansi text.
		</li>
		<li>
			<a href="http://bond.treshna.com">Bond</a> is a rapid application development
			tool that allows you to quickly build database applications for GNOME.
		</li>
		<li>
			<a href="http://www.tecnopro.net/gctb/">gctb</a>
			is a basic accounting system using LIBGDA and GNOME-DB
libraries. The code has a GPL Licence. (web page only in Spanish)
		</li>
		<li>
			<a href="http://www.gnome.org/gnome-office/">GNOME
			Office</a>, a collection of GNOME productivity
			applications
		</li>
		<li>
			<a href="http://glade.gnome.org/">Glade</a>, the
			well-known GTK/GNOME interface builder now supports
			the GNOME-DB widgets
		</li>
		<li>
			<a href="http://www.glom.org">Glom</a> is  a GUI that allows you to
			design database table definitions and the
			relationships between them. It also allows you to edit and search the data in
			those tables. The design is loosely based upon FileMaker Pro, with the added 
			advantage of separation between user interface and data
		</li>
		<li>
			<a href="http://www.gnome.org/projects/gnumeric/">Gnumeric</a>, the
			GNOME Spreadsheet, lets you insert data coming from a database into
			your sheets, by making use of the working-again GDA plugin
		</li>
		<li>
			<a href="http://planner.imendio.org">Planner</a> is
			a project management tool being developed by
			<a href="http://www.imendio.com">Imendio</a>.
		</li>
		<li>
			<a href="http://aspl-fact.sourceforge.net/">ASPL-Fact</a>, is a
			billing system intended for the Spanish-speaking users
		</li>
		<li>
			<a href="http://www.go-mono.com">Mono</a> is using libgda
			for its System.Data.OleDb ADO.NET implementation.
		</li>
		<li>
			<a href="http://gtk-sharp.sourceforge.net">GTK#</a> has C# bindings
			for both libgda and libgnomedb.
		</li>
		<li>
			<a href="http://www.igalia.com/desarrollo/exito/autoarte">AutoArte</a>
			is a distributed management application for car repair
			shops, developed by <a href="http://www.igalia.com">Igalia</a>.
		</li>
		<li>
			<a href="http://www.fisterra.org">Fisterra</a> is an open source
			ERP developed by <a href="http://www.igalia.com">Igalia</a>.
		</li>
		<li>
			<a href="http://sussen.sourceforge.net/">Sussen</a> is a
			 GNOME client for the Nessus Security Scanner. It uses
			 libgda and libgnomedb.
		</li>
		<li>
			The <a href="http://ruby-gnome2.sourceforge.jp">Ruby-GNOME bindings</a>
			contain support for libgda.
		</li>
		<li>
			<a href="http://whm.sourceforge.net/">Warehouse Manager</a>
			is a business application designed to allow you to
			manage several aspects of your warehouse based
			enterprise from your computer.
		</li>
		<li>
			<a href="http://gconta.sf.net">GConta</a> is an
			accounting application.
		</li>
	</ul>
	<h1>Applications used by GNOME-DB/libgda</h1>
	<ul>
		<li>
			<a href="http://www.postgresql.org">PostgreSQL</a> is
			a free RDBMS.
		</li>
		<li>
			<a href="http://www.mysql.org">MySQL</a>, another free
			RDBMS.
		</li>
		<li>
			<a href="http://www.unixodbc.org">unixODBC</a>, a free
			ODBC implementation for UNIX.
		</li>
		<li>
			<a href="http://www.freetds.org/">FreeTDS</a> is a set of
			libraries for Unix and Linux that allows your programs to
			natively talk to Microsoft SQL Server and Sybase databases.
		</li>
		<li>
		<a href="http://www.hwaci.com/sw/sqlite/">SQL
		lite</a>: is a C library that implements an embeddable
		SQL database engine. GNOME-DB includes a provider for
		using this database engine.
		</li>
		<li>
			<a href="http://mdbtools.sourceforge.net">MDB Tools</a>,
			which is a set of tools to manage MS Access files, used
			by the MDB provider.
		</li>
		<li>
			<a href="http://www.openldap.org">OpenLDAP</a> is an open
			source implementation of the <b>L</b>ightweight <b>D</b>irectory
			<b>A</b>ccess <b>P</b>rotocol.
		</li>
	</ul>

<?php
$lastModifiedTime = filemtime('index.php');
html_page_footer($lastModifiedTime);

?>
