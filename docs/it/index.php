<?php
require('html.php');

html_page_header('Documentazione');
?>
<p>
In questa pagina vengono elencati tutti i documenti riguardanti GNOME-DB/libgda disponibili in lingua italiana.

	<h3>Manuali</h3>
	<p>
		<ul>
			<li>
				Manuale di GNOME-DB:
				<a href="manual/gnome-db/index.html">Versione HTML online</a>,
				<a href="manual/gnome-db-manual-html.tar.gz">Tarball HTML</a>,
				<a href="manual/gnome-db-manual.pdf">PDF</a>,
				<a href="manual/gnome-db-manual.ps">PostScript</a>
			</li>
		</ul>
	</p>
<?php
html_page_footer("Jul 02, 2001");

?>
