<?php
require('html.php');

html_page_header('libgda/libgnomedb 1.0.1 released');

?>
<pre>
libgda/libgnomedb 1.0.1 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.

libgda/libgnomedb are the base of the database support in the GNOME Office
application suite, providing database access for many features in both
Gnumeric and Abiword.

This is a bugfix release, containing fixes for various bugs found by users
in the 1.0.0 release.

libgda 1.0.1
------------

 - Fixed clearing of GdaQuarkList's (laurent)
 - Added GType-registration for enums, needed for C++ bindings (murray)
 - Fixed typos in documentation and added more information for
   MySQL provider (laurent)
 - Added missing prototype to gda-client.h (laurent)
 - Fixed detection of mSQL libraries/headers (chris)
 - Fixed mSQL provider handling on non-queries (chris)
 - Fixed #117202: adapted to API changes in tds_connect (seth)
 - Fixed #121403: freed memory being used (rodrigo)
 - Fixed #123342: crash on exit in Gnumeric (rodrigo)
 - Added missing documentation for GdaBlob (rodrigo)
 - Updated translations:
        - cs (miroslav)
	- fi (pauli)
	- fr (christophe)
	- pt (duarte)
	- sk (stanislav)
        - sr (danilo)
	- sr@Latn (danilo)
	- sv (christian)

libgnomedb 1.0.1
----------------

 - Fixed problems with gtksourceview compilation (rodrigo)
 - Made labels non-selectable in gray bar widget (�lvaro)
 - Fill background color with GTK style color in gray bar (�lvaro)
 - Use GTK default text color in grid titles for
   gnome-database-properties (rodrigo)
 - Dont g_free GtkWidget's in gray bar widget finalization (rodrigo)
 - Updated translations:
        - sk (stanislav)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v1.0.1/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
