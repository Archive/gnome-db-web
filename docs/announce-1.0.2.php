<?php
require('html.php');

html_page_header('libgda/libgnomedb 1.0.2 released');

?>
<pre>
libgda/libgnomedb 1.0.2 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.

libgda/libgnomedb are the base of the database support in the GNOME Office
application suite, providing database access for many features in both
Gnumeric and Abiword.

This is a bugfix release, containing fixes for various bugs found by users
in the 1.0.1 release.

libgda 1.0.2
------------

 - Register missing structs as boxed types (murray, laurent)
 - Fixed memory leaks in MySQL provider (paisa)
 - Added more API documentation (laurent)
 - Implemented missing case in gda_data_model_to_xml, where standalone
   XML files were not being generated (laurent)
 - Fixed compilation problems when enabling BSD compatibility layer
   on Linux (jonathan)
 - Fixed gda_value_copy for GdaNumeric values (david)
 - Use $(libdir) as the directory for installing libraries instead of
   $(prefix)/lib (fredreric)
 - Fixes for 64bit platforms (fredreric)
 - Fixed crash in MDB provider (filip)
 - Added missing emission of signals in data model class (gonzalo)
 - Updated translations:
        - cs (miroslav)
        - es (pablo)
	- ja (aihana)
	- nl (vincent)
	- pt (duarte)
	- sr (danilo)

libgnomedb 1.0.2
----------------

 - Avoid creating data sources with duplicating names in GnomeDbDsnConfigDruid
   widget (laurent)
 - Fixes for 64bit platforms (fredreric)
 - Implemented missing gnome_db_combo_set_model (gonzalo)
 - Updated translations:
        - cs (miroslav)
	- el (pkst)
	- fi (pauli)
	- nl (vincent)
	- pt (duarte)
	- sr (danilo)
        - sw (christian)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v1.0.2/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
