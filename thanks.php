<?php
require('html.php');

html_page_header('Thanks', 'thanksto');
?>

<p>
The GNOME-DB project would like to thank sincerely the following collaborations:
<ul>
  <li><b><a href="http://www.hispalinux.es">Hispalinux</a></b>: Our Hosting</li>
  <li><b><a href="http://www.gnome.org">GNOME</a></b></li>
</ul>

<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);

?>
