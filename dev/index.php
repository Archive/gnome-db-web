<?php
require('html.php');

html_page_header('Contribute','contribute');
?>
<p>
So, you want to join the <b>GNOME-DB</b> <a href="people.php">team</a>? There are
several ways to do so. The first of all you can do to help us is to start using
the software. This means try to use it, and report any bug, suggestion,
idea you may have. Then you can help doing any of the following:
<p>
Also, you may want to join our <a href="mailing_lists.php">mailing list</a>,
or drop off by our IRC channel on irc.gnome.org (#gnome-db).

<h2>Writing, completing or translating the documentation</h2>
<table><tr>
<td><img alt="gnome-word" src="../images/gnome-word.jpeg"></td>
<td>    
First, get the latest version of the documents from the <b>doc</b> directory of the <a href="http://cvs.gnome.org/bonsai/rview.cgi?cvsroot=/cvs/gnome&dir=gnome-db">CVS repository</a>. Read it and fix any defects you find, complete it, write new documentation, translate it to a new language, etc.
<p>
We'd love to have a <b>complete user guide</b> you want to start it <a href="mailto:gnome-db-list@gnome.org?subject='I'd like to colaborate writing a user guide'">mail us</a> so that we know that somebody is doing it.
</tr></table>

<h2>Fixing Bugs</h2>
<table><tr>
<td><img alt="gnome-bugbuddy" src="../images/gnome-bugbuddy.jpeg"></td>
<td>
The easiest way to start helping with the code is searching for a bug that is waiting to be fixed 
and fix it. Just have a look at the <a href="http://bugzilla.gnome.org/buglist.cgi?product=gnome-db&product=mergeant&product=libgda&product=libgnomedb&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&email1=&emailtype1=substring&emailassigned_to1=1&email2=&emailtype2=substring&emailreporter2=1&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&short_desc=&short_desc_type=substring&long_desc=&long_desc_type=substring&bug_file_loc=&bug_file_loc_type=substring&status_whiteboard=&status_whiteboard_type=substring&keywords=&keywords_type=anywords&op_sys_details=&op_sys_details_type=substring&version_details=&version_details_type=substring&cmdtype=doit&order=Reuse+same+sort+as+last+time&form_name=query">list of open bugs</a>, choose one, and start working on it. But please, don't forget to <a href="mailto:gnome-db-list@gnome.org">tell us</a> on what you intend to work, so that nobody steps on somebody else's toes.
</td>
</tr></table>

<h2>Making and testing the distribution packages</h2>
<table><tr>
<td><img alt="gnome-pack-deb" src="../images/gnome-pack-deb.jpeg"><br>
<img alt="gnorpm" src="../images/gnorpm.jpeg"></td>
<td>
Our packaging team would love to have some help either making the packages everytime a new release is made or testing it. We currently make packages for:
<ul>
   <li>Debian
   <li>Redhat 6.x and 7.0
</ul>
If you test this packages on other distributions and they work, please <a href="mailto:gnome-db-list@gnome.org">tell us</a>. If they don't it would be great if you volunteer to make them <tt>;-)</tt>
</td>
</tr></table>

<h2>Improving the user interface of the front-end</h2>
<table><tr>
<td><img alt="user interface" src="../images/user-interface.jpeg"></td>
<td>
Any front-end interface can always be improved and having easy to use tools is very important. If you have ideas about how to improve gnome-db UI or have already done so <a href="mailto:gnome-db-list@gnome.org">tell us</a>.
</td>
</tr></table>

<h2>Promoting gnome-db</h2>
<table><tr>
<td><img alt="gnome talk" src="../images/gnome-talk.jpeg"></td>
<td>
<b>You know somebody who is making a gnome application? He/She is accessing databases?</b>. Then tell him/her about gnome-db. He will thank you <tt>;-)</tt>
</td>
</tr></table>

<p>
<b>Any other idea?</b> We'd love to <a href="mailto:gnome-db-list@gnome.org">hear from you</a>

<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
