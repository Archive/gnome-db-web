<?php
require('html.php');

html_page_header('Screenshots','screenshots');
?>
	<p>
		As most people love to see screenshots, here are some so that you can get
		an impression of what the <b>GNOME-DB</b> will look like if you run them
		on your system.
	</p>
	<table border="0" width="100%">
	<tr>
		<td>
			This is the Queries view in Mergeant, which allows users to
			manage the queries. Edition is done through another GUI interface.
		</td>
		<td>
			<a href="/images/screenshots/mergeant_queries.png">
				<img alt="Mergeant's queries view" src="/images/screenshots/mergeant_queries.png" border="0" width="200" height="150">
			</a>
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>
		<td>
			<a href="/images/screenshots/mergeant_query_editor.png">
				<img alt="Mergeant's query editor" src="/images/screenshots/mergeant_query_editor.png" border="0" width="200" height="150">
			</a>
		</td>
		<td>
			This is the query edition dislog in Mergeant, showin here a SQL SELECT query.
			The edition of a query can also be done using only SQL (this mode is activatable
			by using the "SQL" toggle button on the top right).
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>
		<td>
			This is the dialog through which the database relations are shown and some new ones can
			be declared. Any number of relation graphs can be created, so it is possible to create
			graphs showin only a subpart of the database relations.
		</td>
		<td>
			<a href="/images/screenshots/mergeant_relations.png">
				<img alt="Mergeant's database relations graphing" src="/images/screenshots/mergeant_relations.png" border="0" width="200" height="150">
			</a>
		</td>
	</tr>
	<table border="0" width="100%">
	<tr>
		<td>
			<a href="/images/screenshots/login_dialog.png">
				<img alt="login dialog" src="/images/screenshots/login_dialog.png" border="0" width="200" height="150">
			</a>
		</td>
		<td>
			This is the connection dialog, which is included in the <b>GNOME-DB</b>
			library, thus being usable by any application. It offers some extra
			features such as a connection history, which lets you easily select
			any connection you may have used before.
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>
		<td>
			This is Mergeant showing the list of tables for an opened
			connection.
		</td>
		<td>
			<a href="/images/screenshots/mergeant_tables.png">
				<img alt="mergeant tables" src="/images/screenshots/mergeant_tables.png" border="0" width="200" height="150">
			</a>
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>
		<td>
			<a href="/images/screenshots/mergeant_sql_console.png">
				<img alt="mergeant SQL console" src="/images/screenshots/mergeant_sql_console.png" border="0" width="200" height="150">
			</a>
		</td>
		<td>
			This is Mergean't SQL console which replaces the traditional command line clients bundled
			with databases. As a convenience, queries can contain parameters as the 2nd query in this screenshot shows,
			and when executed, the parameters' values are requested and repeated in the console before the query's
			results ("Customer's name => greg" in the screenshot).
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>
		<td>
			This is the data management form in Mergeant, which allows users to
			modify and remove existing records, as well as adding new ones to
			the selected table.
		</td>
		<td>
			<a href="/images/screenshots/mergeant_table_data.png">
				<img alt="mergeant table data" src="/images/screenshots/mergeant_table_data.png" border="0" width="200" height="150">
			</a>
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>
		<td>
			<a href="/images/screenshots/browser_widget.png">
				<img alt="browser" src="/images/screenshots/browser_widget.png" border="0" width="200" height="150">
			</a>
		</td>
		<td>
			This is the database browser widget, which is meant for a quick database's object browse.
			It is just a simple assembly of libgnomedb's widgets.
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>
		<td>
			And here is the configuration application (gnome-database-properties) which
			offers a visual interface to all the GNOME-DB/libgda configuration stuff.
		</td>
		<td>
			<a href="/images/screenshots/manager_component.png">
				<img alt="manager component" src="/images/screenshots/manager_component.png" border="0" width="200" height="150">
			</a>
		</td>
	</tr>
	</table>

	<h1>Old screenshots</h1>	

	<table border="0" width="100%">
	<tr>
		<td>
			<a href="/images/screenshots/gnumeric_gda.png">
				<img alt="gnumeric gda" src="/images/screenshots/gnumeric_gda.png" border="0" width="200" height="150">
			</a>
		</td>
		<td>
			GNOME-DB/libgda is starting to be used in many
			applications. Here's
			<a href="http://www.gnome.org/projects/gnumeric">Gnumeric</a>,
			the GNOME spreadsheet, making use of the GDA plugin,
			which allows you to insert data from a database by
			using libgda.
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>
		<td>
			Here is the GNOME-DB database browser widget being use
			in <a href="http://glade.gnome.org">Glade</a>, the
			GTK/GNOME GUI builder.
		</td>
		<td>
			<a href="/images/screenshots/gnomedb_glade.png">
				<img alt="gnomedb glade" src="/images/screenshots/gnomedb_glade.png" border="0" width="200" height="150">
			</a>
		</td>
	</tr>
	</table>
<?php
  $lastModifiedTime = filemtime('screenshots.php');
  html_page_footer($lastModifiedTime);

?>
