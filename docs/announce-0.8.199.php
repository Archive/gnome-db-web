<?php
require('html.php');

html_page_header('libgda/libgnomedb/mergeant 0.8.199 released');

?>
<pre>
libgda/libgnomedb/mergeant 0.8.199 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite and ODBC data sources.
Mergeant is an end user application that makes use of libgda and libgnomedb to
allow users to easily manage their databases.

This is the final Release Candidate for GNOME-DB 0.9, which will be available,
if no serious problems arise, in a few days after this release.

libgda 0.8.199
--------------

 - GdaValue API additions (rodrigo, vivien)
 - Improved MySQL schemas information (rodrigo)
 - Improvements in FreeTDS provider (holger)
 - Updated documentation (xavier)
 - i18n/UTF8 fixes (carlos)
 - Fixed leaks in configuration API (gonzalo)
 - Added missing implementation for SQLite provider (rodrigo)
 - Added OMF setup (rodrigo, frederic)
 - Added support for error notification on connection opening (rodrigo)
 - Fixed SQLite auto-detection (gonzalo, vladimir)
 - Added man page for gda-config-tool (gonzalo)
 - Added initial support for connection stacks (rodrigo)
 - Added missing methods to Oracle provider (rodrigo)
 - Updated translations:
        - cs (utx)
        - da (olau)
        - de (cneumair)
        - es (pablodc)
	- pl (chyla)
	- sv (menthos)

libgnomedb 0.8.199
------------------

 - Fixed leaks in configuration API (gonzalo)
 - i18n/UTF8 fixes (carlos)
 - Fixed ugliness in error dialog widget (rodrigo)
 - Added OMF setup (rodrigo, frederic)
 - Improved stock icons API, to allow direct access to the stock icons (rodrigo)
 - Fixed initial selection problem in option menus (rodrigo)
 - Fixed #90058 (gonzalo)
 - New stock icons (vivien)
 - Fixed #95959 (rodrigo)
 - Updated translations:
        - cs (utx)
        - da (olau)
        - de (cneumair)
        - ms (sebol)
	- pl (chyla)
	- pt_BR (gdvieira)
	- ru (dmitrym)
	- sv (menthos)

mergeant 0.8.199
----------------

 - Several improvements in data entry and data form widgets, allowing
   now most operations, such as INSERT, UPDATE and DELETE (vivien)
 - Use toolbars for all pages (rodrigo)
 - New unique settings dialog (rodrigo)
 - Fixed #92963 (vivien)
 - UI fixes to conform to the HIG (vivien)
 - i18n/UTF8 fixes (carlos)
 - Added status bar messages for operations on SQL tab (rodrigo)
 - Documentation fixes (rodrigo)
 - Use of GdaValue's for all values (vivien)
 - Implemented DataHandler object (vivien)
 - Made the plugin system use the new DataHandler (vivien)
 - Fixed .desktop file installation (greg)
 - Fixed leaks (gonzalo)
 - Set correctly order of the buttons in dialogs (rodrigo)
 - Associate mergeant icon with mergeant windows (rodrigo)
 - Added OMF setup (rodrigo, frederic)
 - Added new object selector widget (vivien)
 - Improved the query interface (vivien)
 - Updated translations:
        - da (olau)
        - de (chrisime, cneumair)
	- es (pablodc)
        - no (kmaraas)
	- pl (chyla)
	- vi (pablo)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.8.199/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui
* mergeant: libgda/libgnomedb and dependencies

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
