<?php
require('html.php');

html_page_header('GNOME-DB 0.1.0 released');

?>
<pre>
GNOME-DB 0.1.0 (a.k.a. olympius) has been released.

GNOME-DB is a complete framewok for developing database-oriented
applications, and actually allows access to PostgreSQL, MySQL,
Oracle and ODBC data sources. Apart from this development framework,
it also includes several applications and utilities which make it
quite suitable for many database-related taks.
This new release is the long-awaited 0.1.x, and includes:

* the GNOME and non-GNOME parts have been separated. This results in two
  modules: libgda, which is the data-access framework, and which is now
  almost GNOME-independent, and gnome-db, which continues to be the
  GNOME specific part, that is, a frontend to libgda.
* as part of the separation, the GNOME-specific part has started its full
  GNOMEification, making full use of the GNOME technologies, such as
  GConf, Bonobo, etc. Plans are on integrating with GNOME Office apps.
* PostgreSQL provider: the PostgreSQL provider has been fixed to be able
  to work with both PostgreSQL 6.x and 7.x, where major changes were made
  in the dictionary tables. Thus, both servers can be accessed through
  an unique binary.
* Full GConf support: all gnome_config* API calls have been removed in
  favor of GConf, the new GNOME configuration system.
* gda-test program, which lets you test your libgda configuration (in source
  tarball only).

GNOME-DB is now a GNOME-next-generation application, and makes use of the
latest GNOME developments, so you will need a huge list of packages in
your system to make it work. A recent version of GNOME (Helix GNOME) is
suggested. Apart from this, you'll need OAF (0.5.x), Bonobo (0.18.x),
GtkHTML (0.6.x), GConf (0.6.x). Note that some latest versions of these
packages may not be compatible. Mainly, GConf-0.9.x and Bonobo > 0.18.x
(that is, the CVS version).

You can download the package and find other sources of information
in http://www.gnome.org/projects/gnome-db/.
</pre>
<?php

html_page_footer('');

?>
