<?php
require('html.php');
function read_line ($handle)
{
	if (feof ($handle))
		return FALSE;

	return fgets ($handle, 2048);
}

function print_lines ($handle, $stop_on, $dont_print = FALSE)
{
	$len = strlen ($stop_on);
	$first_non_empty = FALSE;
	while (($line = read_line ($handle)) != FALSE) {
		if (!strncmp ($line, $stop_on, $len))
			break;

		if ($dont_print == TRUE)
			continue;

		$trimmed = trim ($line);
		if ($first_non_empty == FALSE && strcmp ($trimmed, "") != 0)
			$first_non_empty = TRUE;

		if ($first_non_empty == FALSE)
			continue;

		$output = htmlentities ($trimmed);
		$output = ereg_replace ("#([0-9]+)", "<a href=\"http://bugzilla.gnome.org/show_bug.cgi?id=\\1\">\\0</a>", $output);
		print  $output . "<br>\n";
	}

	if ($dont_print == FALSE && $first_non_empty == FALSE)
		print "<br>\n";
}

function NewEntry ($handle)
{
	print "<tr>\n";
}

function Module ($handle)
{
	$dont_print = FALSE;
	$line = read_line ($handle);
	if ($line == FALSE)
		$dont_print = TRUE;
	
	$module = $_GET["module"];
	if ($module == "")
		$module = 'All';

	if ($module != 'All' && strncmp ($line, $module, strlen ($module)))
		$dont_print = TRUE;
	
	if ($dont_print == FALSE) {
		NewEntry ($handle);
		print '<td align="center"><font size="-1">';
		print $line;
		print '</font></td>';
	}
	read_line ($handle);
	return $dont_print;
}

function Changes ($handle, $dont_print = FALSE)
{
	if ($dont_print == FALSE)
		print '<td align="center"><font size="-1">';

	$line = read_line ($handle);

	if ($dont_print == FALSE) {
		$tok = strtok ($line, " \n\t");
		print "Author: $tok<br>\n";
		$tok = strtok (" \n\t");
		print "Date: $tok<br>\n";
		$tok = strtok (" \n\t");
		print "Time: $tok<br>\n";
		print '</font></td>';
	}
	
	read_line ($handle); // Ignores -LogMessage-
}

function Rest ($handle, $dont_print = FALSE)
{
	if ($dont_print == FALSE)
		print '<td><font size="-1">';

	print_lines ($handle, '-LogMessage-', $dont_print);
	if ($dont_print == FALSE)
		print '</font></td>';
}

function LogMessage ($handle, $dont_print = FALSE)
{
	if ($dont_print == FALSE)
		print '<td><font size="-1">';

	print_lines ($handle, '-URL-', $dont_print);

	if ($dont_print == FALSE)
		print '</font></td>';
}

function URL ($handle, $dont_print = FALSE)
{
	$line = read_line ($handle);
	if ($dont_print == TRUE)
		return;

	print '<td align="center"><font size="-1">';
	print '<a href="' . htmlspecialchars (trim ($line)) . '">View diffs</a>';
	print '</font></td>';
}

function EndEntry ($handle, $dont_print = FALSE)
{
	if ($dont_print == FALSE)
		print "</tr>\n";
}

function ProcessData ($handle)
{
	$printed = 0;
	while ($printed < 100 && ($line = read_line ($handle)) != FALSE) {
		if (!strncmp ($line, '--New entry--', 13))
			continue;

		if (strncmp ($line, '-Module-', 8))
			continue;

		$dont_print = Module ($handle);
		Changes ($handle, $dont_print);
		Rest ($handle, $dont_print);
		LogMessage ($handle, $dont_print);
		URL ($handle, $dont_print);
		EndEntry ($handle, $dont_print);
		if ($dont_print == FALSE)
			$printed++;
	}
}

function create_option ($selected, $option_name)
{
	if ($selected == $option_name)
		$selected = ' selected';
	else
		$selected = '';

	print "<option value=\"$option_name\"$selected>$option_name</option>\n";
}

html_page_header('Mergeant / libgnomedb / libgda latest 100 commits', 'commits');
?>

<tr><td bgcolor="#FFFFFF">
<form name="theForm" method="get" action="<?php print $_SERVER['PHP_SELF']; ?>">
<hr>
Select a module name to filter on: &nbsp;
<select name="module" id="module">
	<?php
	
	$module = $_GET ["module"];
	if ($module == "")
		$module = 'All';

	create_option ($module, 'All');
	create_option ($module, 'libgda');
	create_option ($module, 'libgnomedb');
	create_option ($module, 'mergeant');
	create_option ($module, 'gnome-db-web');
	?>
</select>
&nbsp;
<input type="submit" name="filter" value="Filter" id="filterButton" />
</form>
<hr>
</td></tr>
<tr><td>

<table summary="latest commits" border="1" cellspacing="1" cellpadding="5" bgcolor="#FFFFFF">
<thead>
<tr>
<th>Module</th>
<th>Author and date <font size="-2">UTC-4</font></th>
<th>Modified files</th>
<th>Log message</th>
<th>Link</th>
</tr>
</thead>
<tbody>
<?php
	$handle = fopen ('/home/web/gnome-db.org/include/data/commit-list', 'r');
	if ($handle != FALSE) {
		ProcessData ($handle);
		fclose ($handle);
	}
?>
</tbody>
</table>

<?php
  $lastModifiedTime = filemtime('commits.php');
  html_page_footer($lastModifiedTime);
?>

