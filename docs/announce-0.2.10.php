<?php
require('html.php');

html_page_header('GNOME-DB 0.2.10 released');

?>
<pre>
GNOME-DB/libGDA 0.2.10 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
they also include several applications and utilities which make them
quite suitable for many database-related taks.

This release is mainly a bugfix release, but which includes fixes for
important bugs found in 0.2.9.

Changes since last version:

	* fixed the bug that made first tries to open a connection always
	  fail (Rodrigo)

	* now providers are not left behind anymore and they terminate
	  themselves when no more connections are open (Rodrigo)

	* also, components are not left behind anymore, since now they
	  know at each moment how many clients there are connected, so
	  they terminate themselves when the last client disconnects
	  (Rodrigo)

	* error dialog box is no longer shared between connections, thus
	  avoiding weird things such as getting an error message coming
	  from a different provider than the one issuing the error (Rodrigo)

	* some UI tweaks (Rodrigo)

Now in development:

	* Export/Import database components based on a new DATABASE XML format
	* Perl scripting support in the front-end
	* CORBA-based report engine
	* full GLib 1.3/2.0 porting

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml1
* gnome-db: libgda and dependencies, Bonobo >= 1.0.0, gnome-libs

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
