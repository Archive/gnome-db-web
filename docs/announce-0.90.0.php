<?php
require('html.php');

html_page_header('libgda/libgnomedb 0.90.0 released');

?>
<pre>
libgda/libgnomedb 0.90.0 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.

This release marks the beginning of the end of the normal development
process, since this is one of the latest releases before the 1.0 beta
testing cycle starts.
This release also does not include Mergeant, which has been detached
from the libgda/libgnomedb release process to allow us to get a 1.0
version of those 2 libraries for a (hopefully) GNOME Office release
during this summer. Since Mergeant still needs some work to be in
a 1.0 state, we will continue its development as we've been doing
all this time, but it will keep separate version numbers and (possibly)
schedules than libgda/libgnomedb. Once we get libgda/libgnomedb 1.0 out
of the door, we will concentrate on making Mergeant 1.0 as good as we
all want it to be.

Changes in this release since 0.12.1:

libgda 0.90.0
-------------

 - Added missing stanzas for MDB and LDAP providers in spec file (adam)
 - Added version 3 support for the mSQL provider (dschoene)
 - Added support for unsigned integer values (dschoene)
 - Added DONT_SHARE option for connections (rodrigo)
 - Some more documentation (rodrigo)
 - Updated translations:
        - cs (mitr)
	- es (pablodc)
        - ml (karunakar)
	- nl (vincent)
	- pt (dnloreto)
	- ru (frob)
        - zh_TW (baddog)

libgnomedb 0.90.0
-----------------

 - Added missing entry points to GnomeDbLoginDialog API (rodrigo)
 - Removed non-working MIME components (rodrigo)
 - Added MIME actions component for SQL and connection files (rodrigo)
 - Removed dependencies on GTK+ 2.2 (rodrigo)
 - Added missing files to spec file (adam)
 - Added preloading of GConf keys (rodrigo)
 - Catched missing translatable strings (baddog)
 - HIG-ify the error dialog widget (rodrigo)
 - Updated translations:
        - be (dmitrym)
        - cs (mitr)
	- es (pablodc)
        - ml (karunakar)
        - nl (vincent)
	- pt (dnloreto)
	- ru (frob)
        - sv (menthos)
        - zh_TW (baddog)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.90.0/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
