<?php
require('html.php');

html_page_header('libgda/libgnomedb/gnome-db 0.8.192 released');

?>
<pre>
libgda/libgnomedb/gnome-db 0.8.192 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite and ODBC data sources.
gnome-db is a set of visual applications that let you access from a nice
UI all the features provided in libgda/libgnomedb.

This is another preview release of the GNOME 2 versions of these 3 projects, and
likely to be the last one before the final gASQL/gnome-db merging, which is
now showing in your nearest CVS server.

libgda 0.8.192
--------------

 - Some work on the report engine (carlos)
 - More API documentation (gonzalo, rodrigo)
 - Added gda_connection_exec_non_query to GdaConnection API (rodrigo)
 - Added get_database virtual method on providers (rodrigo)
 - Added change_database virtual method on providers (rodrigo)
 - Added command line tool for libgda configuration (gonzalo)
 - Fixed many config API problems (gonzalo)
 - Allow storing passwords in data sources (rodrigo)
 - Added GdaTransaction class for a better use of transactions (rodrigo)
 - Added gda_client_open_connection_from_string to GdaClient API (rodrigo)
 - Re-added PARENT_TABLES schema from the GNOME 1.4 branch (rodrigo, gonzalo)
 - First working version of the Oracle provider for the new API (tim)
 - Fixed PostgreSQL tests (gonzalo)
 - Updated SQLite provider to the latest changes (carlos)
 - Added SQL parser (andru)
 - Improved error management in GdaConnection class (gonzalo)
 - Updated translations:
 	- da (olau)
 	- es (pablodc)
	- ko (cwryu)
	- no (kmaraas)
	- pl (chyla)
	- pt (dnloreto)
	- sk (stano)
	- sv (menthos)
	- sl (minmax)
	- vi (pablo)

libgnomedb 0.8.192
------------------

 - Fixed parallel installation of pixmaps (tagoh)
 - Fixed parallel installation of Bonobo components (tagoh)
 - Fixed schema files for translation (rodrigo)
 - Added control center applet for database connections config (rodrigo)
 - Extended GnomeDbGrid API (rodrigo)
 - Allow edition of passwords associated with data sources (rodrigo)
 - Small compilation fixes (damon)
 - New & improved GnomeDbBrowser widget (rodrigo, gonzalo)
 - Fixed English sentences (kmaraas)
 - Fixed crash when exiting gnome-db-components (gonzalo)
 - Added gnome_db_config API based on Gconf and synchronized with the non-GConf
   API in libgda (gonzalo)
 - Fixed displaying of errors in GnomeDbError widget (gonzalo)
 - Updated translations:
 	- da (olau)
	- es (pablodc)
	- ko (cwryu)
 	- no (kmaraas)
 	- pl (chyla)
	- pt (dnloreto)
 	- sk (stano)
	- sl (minmax)
	- sv (menthos)�

gnome-db 0.8.192
----------------

 - Removed manager component, as all configuration is all managed by the
   libgnomedb's control center applet (rodrigo)
 - Fixed problems with icons and Bonobo (gonzalo)
 - Updated translations:
 	- no (kmaraas)
 	- pl (chyla)
 	- sl (minmax)
 	- sv (menthos)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.8.192/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui
* gnome-db: libgda/libgnomedb and dependencies

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
