<?php
require('html.php');

html_page_header('libgda/libgnomedb/gnome-db 0.8.105 released');

?>
<pre>
libgda/libgnomedb/gnome-db 0.8.105 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase, SQLite and ODBC data sources. Apart from this development
gnome-db is a set of visual applications that let you access from a nice
UI all the features provided in libgda/libgnomedb.

This is yet another snapshot release for the different GNOME 2 alphas/betas.

libgda 0.8.105
--------------

 - Added Numeric type to IDL (mike, rodrigo)
 - Implemented SCHEMA_FIELDS for the PostgreSQL provider (gonzalo)
 - Improved and made GdaXmlDatabase usable (rodrigo)
 - Started new default provider, based on GdaXmlDatabase (rodrigo)
 - Fixed parameters management (gonzalo, rodrigo)
 - Fixed cleaning up of error list in providers (rodrigo)
 - Improved performance for type mapping in PostgreSQL provider (gonzalo)
 - Fixed referencing in GdaClient (rodrigo)
 - Fixed wrong return of errors in PostgreSQL provider (gonzalo)
 - Added support for exporting data models to comma and tab separated
   files (rodrigo)
 - Implemented SCHEMA_AGGREGATES in MySQL provider (rodrigo)
 - Implemented SCHEMA_FIELDS in MySQL provider (rodrigo)
 - Fixed some crashes in the MySQL provider (rodrigo)
 - Updated translations:
 	- de (chrisime)
 	- ms (sebol)
	- pl (chyla)
	- ru (frob)
	- sk (stano)
	- uk (rasta)
	- zh_CN (lark)

libgnomedb 0.8.105
------------------

 - Improved Yes/No dialog (rodrigo)
 - Fixed reference problems with BonoboWidget's (rodrigo)
 - Extended GnomeDbBrowser API (rodrigo)
 - Fixed selections in GnomeDbGrid and GnomeDbList (rodrigo)
 - Made GnomeDbGrid display BOOLEAN values as check boxes (rodrigo)
 - Display numbers correctly in GnomeDbGrid (rodrigo)
 - Use check buttons for BOOLEAN values in GnomeDbForm (rodrigo)
 - Added basic support for VIEWS in GnomeDbBrowser (rodrigo)
 - Fixed displaying of column titles in tree models (gonzalo)
 - Added 'Save us' menu item to the popup menu for GnomeDbGrid,
   which allows to save the displayed data to different formats,
   such as comma or tab-separated files, XML, etc (rodrigo)
 - New icons (apg)
 - New GnomeDbSqlEditor widget, with syntax hightlighting, multiple
   command support, etc (daniel)
 - Updated translations:
 	- de (chrisime)
	- no (kmaraas)
	- pl (chyla)
	- ru (frob)
 	- sk (stano)

gnome-db 0.8.105
----------------

 - New 'View' menu items in database browser (rodrigo)
 - Show progress messages for all database operations (rodrigo)
 - Make sure we close all open connections on exit (rodrigo)
 - Fixed connection properties dialog (rodrigo)
 - Added support for configuration controls for all components (rodrigo)
 - Added basic settings dialog (rodrigo)
 - Use GnomeDbSqlEditor for all SQL editing (daniel)
 - Updated translations:
 	- da (olau)
	- de (chrisime)
	- no (kmaraas)
	- pl (chyla)
	- ru (frob)
	- sk (stano)
	- zh_CN (lark)

To install this new version, you'll need:
* libgda: ORBit2, bonobo-activation, GConf, libxml2, libbonobo, gnome-vfs
* gnome-db: libgda and dependencies, libgnome/ui, libglade.

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
