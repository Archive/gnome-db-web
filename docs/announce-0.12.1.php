<?php
require('html.php');

html_page_header('libgda/libgnomedb/mergeant 0.12.1 released');

?>
<pre>
libgda/libgnomedb/mergeant 0.12.1 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.
Mergeant is an end user application that makes use of libgda and libgnomedb to
allow users to easily manage their databases.

This release contains fixes for some important bugs found in 0.12.0.

Changes in this release since 0.12.0:

libgda 0.12.1
-------------

 - Fixed leak in Oracle provider to prevent running out of cursors (fozzy)
 - Fixed normal and build requirements in spec file (david)
 - Updated translations:
        - nl (adrighem)
        - sv (menthos)

libgnomedb 0.12.1
-----------------

 - Added missing static libraries to the spec file (adam)
 - Fixed typo in configure.in (redfox)
 - Simplified column titles in data sources and providers config dialog (rodrigo)
 - Added missing CFLAGS for libglade module (rodrigo)
 - Fixed soname numbering (rodrigo)
 - Updated translations:
        - nl (adrighem)
        - sr (danilo)
	- ta (baddog)

mergeant 0.12.1
---------------

 - Added missing plugins directory to spec file (adam)
 - Fixed crash in #113623 (rodrigo)
 - Fixed main window resizing problems (rodrigo)
 - Updated translations:
        - fr (redfox)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.12.1/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview
* mergeant: libgda/libgnomedb and dependencies

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
