<?php
require('html.php');

html_page_header('GNOME-DB/libgda/gASQL 0.2.94 released');

?>
<pre>
GNOME-DB/libGDA/gASQL 0.2.94 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase, SQLite and ODBC data sources. Apart from this development
framework, they also include several applications and utilities which make
them quite suitable for many database-related taks.
gASQL is a database administration tool based on libgda and GNOME-DB, featuring
visual tools for managing any database supported by libgda/GNOME-DB.

Changes:

	* API changes: gnome_db_grid_set_selection_mode has been added, to allow
	  apps to specifiy the selection mode for GnomeDbGrid widgets (Juan)

	* C++ libgda bindings improvements (Kuba)

	* Fixed inconsistency in some .h and .c libgda files (Rodrigo)

Bugs fixed:

	* fixed documentation building (Rodrigo)

	* fixed freezing problems with unsupported data types (Waldemar)

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml1, Bonobo >= 1.0.x
* gnome-db: libgda and dependencies, gnome-libs

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
