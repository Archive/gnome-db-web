<?php
require('html.php');

html_page_header('Download','download');
?>
	<p>
		There are several ways you can download GNOME-DB/libgda, depending on
		what use you're going to make out of it. If you're helping on the
		development of GNOME-DB, you definitely want the development
		version, which is available on the <a href="http://cvs.gnome.org">GNOME
		CVS server</a>. If you're a user, you'd prefer the stable release.
	<p>
		In either case, you will need the following stuff installed on your system:
		<ul>
			<li>
				libgda
				<ul>
					<li><a href="http://www.gtk.org">glib</a></li>
					<li>libxml</li>
					<li>libxslt</li>
					<li>(Optionally) DBMS libraries (PostgreSQL, Oracle, MySQL...)</li>
				</ul>
			</li>
			<li>
				libgnomedb
				<ul>
					<li>libgda and dependencies</li>
					<li>gtk, gnome-libs, libglade, bonobo</li>
					<li>(Optionally) GtkSourceView</li>
				</ul>
			</li>
			<li>
				Mergeant	
				<ul>
					<li>libgnomedb and dependencies</li>
				</ul>
			</li>
		</ul>
	<h3>Stable version</h3>
	<p>
		Stable versions are released when some objectives are reached in the
		development, and so are more tested than development versions. They are
		available in different formats:
		<ul>
			<li><a href="http://ftp.gnome.org/pub/GNOME/sources/libgda/">libgda Source tarballs</a></li>
			<li><a href="http://ftp.gnome.org/pub/GNOME/sources/libgnomedb/">libgnomedb source tarballs</a></li>
			<li><a href="http://ftp.gnome.org/pub/GNOME/sources/mergeant/">mergant source tarballs</a></li>
			<li>Debian packages: packages are now part of the
			    <a href="ftp://ftp.debian.org">official</a> distribution.</li> 
			<li>Mandrake packages: libgda and libgnomedb are now part of
			    <a href="http://www.mandrakelinux.com/en/cookerdevel.php3">the
			    Cooker unstable release</a>.
		</ul>
	<h3>Development version</h3>
	<p>
		To be on the bleeding edge of GNOME-DB development, you'll want to
		use the GNOME CVS server. For doing so, you just have to follow the instructions
		below.
	<p>
		The first time, you'll have to authenticate yourself with the server. For that:
		<pre>
	cvs -z3 -d :pserver:anonymous@anoncvs.gnome.org:/cvs/gnome login
		</pre>
		it will ask you for a password. When so, just press <code>ENTER</code>, since
		there is no password.
	<p>
		Next step is to obtain a copy of the sources:
		<pre>
	cvs -z3 -d :pserver:anonymous@anoncvs.gnome.org:/cvs/gnome co libgda libgnomedb mergeant
		</pre>
		This will download the libgda, libgnomedb and mergeant sources. To compile, first of all
		you'll need to compile libgda, then libgnomedb and then, once installed, compile and install
		mergeant.
	</p>

<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
