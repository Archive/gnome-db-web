<?php
require('html.php');

html_page_header('libgda/libgnomedb/mergeant 0.9.0 released');

?>
<pre>
libgda/libgnomedb/mergeant 0.9.0 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase and ODBC data sources.
Mergeant is an end user application that makes use of libgda and libgnomedb to
allow users to easily manage their databases.

This 0.9 release is the result of more than a year of refactoring and porting to
the GNOME 2.0 framework, which has resulted in a more comprehensive set of
tools and libraries.

libgda has seen all its CORBA dependencies removed, being now based on a simple
plugin architecture, which makes it suitable for any kind of environment. Also,
a more comprehensive way of accessing data has been added, resulting in the
GDA data models, which allow a highly abstracted access to the data returned
by the underlying RDBMS.
libgnomedb continues to be a rich set of data oriented/bound widgets, but now with
a MVC-based architecture which allows a better management of data in GNOME GUI
applications.
Mergeant is the result of merging gASQL (http://gasql.sourceforge.net) and the old
GNOME-DB frontend. And, believe it, this is one of the best decisions we've ever made,
since Mergeant adds to the suite a rich set of UI tools for managing data via
libgda/libgnomedb.

This release also marks the beginning of the "road to 1.0", which is what GNOME-DB
developers are already working on, and which will include all the features currently
missing in 0.9.

Changes in this release since 0.8.199:

libgda 0.9.0
------------

 - Fixed weird initialization of the MySQL structures (cleber)
 - Fixed #96758 (rodrigo)
 - Fixed #95985 (rodrigo)
 - Fixed #96810 (rodrigo)
 - Fixed bison problems in SQL parser (gonzalo)
 - Fixed DTD validation of config XML file (gonzalo)
 - Implemented most schemas for FreeTDS provider (holger)
 - Fixed crash on provider lookups in client library (gonzalo)
 - Documentation fixes and additions (gonzalo, calvaris)
 - Fixed #99997 (rodrigo)
 - Fixed #96810 (rodrigo)
 - First working version of the ported (from libgda 0.2.x) Sybase
   provider (holger)
 - Fixed #97669 (holger (FreeTDS part), rodrigo (Oracle part))
 - Updated translations:
        - cs (Michal Bukovjan)
        - de (cneumair)
        - es (pablodc)
        - ms (sebol)
        - pt_BR (gdvieira)
        - sv (menthos)

libgnomedb 0.9.0
----------------

 - Splitted stock icons initialization from the main library init function,
   to have the GnomeDbLogin/GnomeDbLoginDialog widgets work correctly
   in Glade (rodrigo)
 - Updated translations:
        - be (dmitrym)
        - cs (Michal Bukovjan)
        - de (cneumair)
        - es (pablodc)
        - ms (sebol)
        - nl (adrighem)
        - pt_BR (gdvieira)
        - sv (menthos)

mergeant 0.9.0
--------------

 - Replaced old ChoiceCombo with the new ItemSelector (vivien)
 - Improved queries edition (vivien)
 - Fixed #96838 (rodrigo)
 - Fixed #96842 (rodrigo)
 - Fixed #96844 (rodrigo)
 - Fixed #96836 (rodrigo)
 - Added integrated settings dialog (rodrigo)
 - Updated translations:
        - cs (Michal Bukovjan)
        - de (cneumair)
        - es (pablodc)
        - fr (redfox)
        - ms (sebol)
        - nl (adrighem)
        - sv (menthos)
        - vi (pablo)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.9.0/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui
* mergeant: libgda/libgnomedb and dependencies

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
