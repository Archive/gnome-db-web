<?php
require('html.php');

html_page_header('GNOME-DB 0.2.9 released');

?>
<pre>
GNOME-DB/libGDA 0.2.9 have been released.

GNOME-DB/libGDA are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL,
Oracle, Sybase and ODBC data sources. Apart from this development framework,
they also include several applications and utilities which make them
quite suitable for many database-related taks.

This new release is a big step for the general usability of GNOME-DB/libgda,
with new features and a new look&feel. Note that this is the first release
of GNOME-DB/libgda to be ready for end users (not as the previous ones,
which were almost only ready for the GNOME-DB/libgda developers :-)
There are some incomplete things though, essentially because this is a release
made to show all the new additions since last release. Things will be
completed in subsequent preview releases (0.2.x), and everything will be
hopefully working in 0.3.0. 

Changes since last version:

	* new gda-default provider: apart from being able to access remote
	database systems (PostgreSQL, MySQL, Oracle, etc), GDA now allows
	the creation/management of personal databases. The gda-default
	provider uses an embedded database system 
	(<a href="http://www.hwaci.com/sw/sqlite">SQLite</a>) which makes
	it very easy for users to create new databases, since there is no
	need any more of first setting up a database server. This is similar
	to dBase or MS Access file formats. But, while being primarily
	oriented for end users, the gda-default provider is also useful
	for power DB users, since it allows you to use it as a prototype
	tool as well as for small applications, where there is no need
	for a full-featured RDBMS. This is possible since the gda-default
	provider (well, SQLite) supports a big subset of the SQL language,
	along with other useful features, such as transactions, indices
	(Rodrigo)

	* new data source wizard: the previous UI for setting up data
	sources was very confusing, so we've rewritten it, and now it's as
	easy as running a wizard which guides you through all the process
	(Rodrigo, wizard design by Joaquin Cuenca)

	* new database wizard: a new dialog has been added to let the users
	easily create new databases. For the moment, it just allows the
	creation of databases managed by the gda-default provider, but work
	is on the way to add it support for all providers, and also to
	allow users to create a database from a template (Rodrigo)

	* libxmlquery: we've got now a full implementation of the XML query
	format, which will allow clients to send commands in a special
	XML format, thus providing the commands' portability between
	different providers. In next versions, graphical tools around
	this feature will be available (Gerhard)

	* conversion to BonoboXObject: now, all components are based on
	BonoboXObject (Michael)

	* don't distribute ORBit-generated files in tarball (Michael)

	* lots of bugs fixed (help from many bugzilla users)

	* Spanish translation of the GNOME-DB/libgda manuals (David)

	* Portuguese translation of the GNOME-DB manual (Cleber)

	* new/updated translations (GNOME Translation Team)

	* beginning of the API reference (20% of functions documented :-)
	(Rodrigo)

	* new spec files (Serge)

	* Gnumeric GDA plugin: it now works, allowing you to insert data
	from a database (obtained via libgda) into your Gnumeric files.
	More work is needed to make it more useful though (Rodrigo)

Now in development:

	* Export/Import database components based on a new DATABASE XML format
	* Perl scripting support in the front-end
	* CORBA-based report engine
	* full GLib 1.3/2.0 porting
	* Event signals from the providers, to allow clients to listen for
	events (open_connection, exec_command, begin_transaction, etc)

To install this new version, you'll need:
* libgda: ORBit, OAF, GConf, libxml1
* gnome-db: libgda and dependencies, Bonobo >= 1.0.0, gnome-libs

You can find more information about GNOME-DB/libGDA at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
