<?php
require('html.php');

html_page_header('libgda/libgnomedb 0.99.0 released');

?>
<pre>
libgda/libgnomedb 0.99.0 have been released.

libgda/libgnomedb are a complete framewok for developing database-oriented
applications, and actually allow access to PostgreSQL, MySQL, Oracle, Sybase,
SQLite, FireBird/Interbase, IBM DB2, mSQL and MS SQL server, as well as
MS Access and xBase files and ODBC data sources.

This release is RC1 for the final 1.0 release, so it should be almost
identical as the final 1.0.

Changes in this release since 0.91.0:

libgda 0.99.0
-------------

 - Fixed typo to build the freetds correctly (tagoh)
 - Added gda_config_save_data_source_info function (rodrigo)
 - Manage correctly non-SELECT statements in MySQL provider (gonzalo, csilles)
 - Added Blob API (juan-mariano, gonzalo)
 - Fixed many issues with GdaXmlDatabase (philippe, rodrigo)
 - Fixed leaks in PostgreSQL provider (gonzalo)
 - Added more API documentation (rodrigo)
 - Added missing SQL support in XML provider (rodrigo)
 - Updated translations:
        - ca (jordim)
        - cs (mitr)
	- de (cneumair)
        - es (pablodc)
        - fi (pvirtane)
        - fr (redfox)
	- it (marcoc)
	- no (kmaraas)
	- pl (aflinta)
	- pt (dnloreto)
	- sl (minmax)
	- sr (danilo)
	- sv (menthos)

libgnomedb 0.99.0
-----------------

 - Added a (configurable) icon to GnomeDbGrayBar (apg)
 - Use icons for gnome-database-properties tabs (rodrigo)
 - Removed ugly BUILD_WITH_GNOME in public headers (daniel)
 - Updated translations:
        - ca (jordim)
        - cs (mitr)
	- de (cneumair)
        - es (pablodc)
        - fi (pvirtane)
	- fr (redfox)
	- no (kmaraas)
	- pl (aflinta)
	- pt (dnloreto)
	- sl (minmax)
        - sr (danilo)
	- sv (menthos)

Tarballs are available at ftp://ftp.gnome-db.org/pub/gnome-db/sources/v0.99.0/

To install this new version, you'll need:
* libgda: glib, libxml2, libxslt
* libgnomedb: libgda and dependencies, libgnome/ui, libglade, libbonoboui and, optionally, gtksourceview

You can find more information at the projects' homepage
(http://www.gnome-db.org), or you can ask any question/propose anything you
want in the GNOME-DB mailing list, which is available at
http://mail.gnome.org/mailman/listinfo/gnome-db-list.
</pre>
<?php
  $lastModifiedTime = filemtime('index.php');
  html_page_footer($lastModifiedTime);
?>
